import os

def print_name():
	for i in range(0, 178):
		# format to be printed to image_names.txt:
		yield("###urIMG{:04d}.png\t###urIMG{:04d}_a.png\n".format(i,i))


for p in print_name():
	string = p.split("#")
	# isolate name of annotated & rgb images:
	annotated = string[3].strip("\t")
	rgb = string[6].strip("\n")
	if os.path.isfile(annotated) & os.path.isfile(rgb):
		with open("image_names.txt", "a") as file:
			file.write(p)
	else:
		if not os.path.isfile(annotated):
			print(annotated + " does not exist")
		if not os.path.isfile(rgb):
			print(rgb + " does not exist")