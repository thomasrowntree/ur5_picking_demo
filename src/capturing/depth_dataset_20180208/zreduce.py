#!/usr/bin/env python
import argparse
import os
import sys
import cv2
import numpy as np

i = 0
for filename in sorted(os.listdir(os.path.dirname(os.path.realpath(__file__)))):
    print i
    image = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
    threeChannel = np.zeros((480,640,3), np.uint8)
    threeChannel = cv2.cvtColor(image,cv2.COLOR_GRAY2RGB)
    smallArray = threeChannel[0:480,165:(640-150)]
    cv2.imwrite( os.path.splitext(filename)[0]+"_small"+".png", smallArray );
    i = i + 1
