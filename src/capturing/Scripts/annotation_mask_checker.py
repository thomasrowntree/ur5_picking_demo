#!/usr/bin/env python

import os
import sys
import errno
import cv2
import numpy as np

# This script is usefull for checking that images have been annotated correctly.
# It will iterate over every image and use the map to cut out each item type and
# save them in their own folders. This means for example that all the cut outs of
# soccer_balls will be in one folder and cut outs of cars will be in another.
# Its becomes easy to spot a bad annotation because a tree could end up in the cars
# folder

# path to the file names text file. This file should contain relative file paths
# pairs to the rgb and map images. The file name pairs must be tab seperated
# eg.
# dataset/rgb_00001.png  dataset/map_00001.png
# dataset/rgb_00002.png  dataset/map_00002.png
data_file_path = "../dataset_20180208/image_names.txt"

# sub folders for each item type will be created under this folder and then
# populated with images of the same type.
# eg.
#   base_out_path/
#       car/
#           img_0001.png
#           img_0002.png
#       tree/
#           img_0003.png
#           img_0004.png
base_output_path = "/home/thomas/datasets/annotation_check"

# specify the text lables for each of the indexs. These labels will be used
# for the folder names. If an index is unlabled then the index number will be used
index_names_dict = {
0: "background",
1: "hands",
2: "poo",
3: "chips",
4: "monkey",
5: "poonicorn",
6: "pig",
7: "ball",
8: "die",
9: "rope",
10: "ring",
11: "croc",
12: "bb8"
}


#Iterate over every line in the data_file and join the path to the data_file with
# the relative paths to the images.
rgb_map_path_pairs_list = []
#get the path the dir that contains data_file
data_file_dir_path = os.path.dirname(data_file_path)

#open the file
with open(data_file_path) as data_file:
    #read all the lines
    lines = data_file.readlines()

    for line in lines:
        #strip out new lines and spaces then seperat by tabs
        rgb_path,map_path = line.strip().split('\t')

        #join the relative image paths to the dir that this data file is in
        rgb_path = os.path.join(data_file_dir_path,rgb_path)
        map_path = os.path.join(data_file_dir_path,map_path)

        #if the image path does not exits, tell the user and continue to the next line
        if not os.path.exists(rgb_path):
            print("Does not exists: %s" % rgb_path)
            continue

        #if the image path does not exits, tell the user and continue to the next line
        if not os.path.exists(map_path):
            print("Does not exists: %s" % map_path)
            continue

        #If the files exist then append the pair of file paths
        rgb_map_path_pairs_list.append((rgb_path,map_path))





def ensureDirPathExists(dirpath):
    """Try to create the directory and catch the error only if it already exists"""
    try:
        os.makedirs(dirpath)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


count = 0
#for each rgb and map path pair
for rgb_path, map_path in rgb_map_path_pairs_list:

    #load the images.
    rgb_img = cv2.imread(rgb_path)
    map_img = cv2.imread(map_path,cv2.IMREAD_GRAYSCALE)

    rgb_name = os.path.basename(rgb_path)
    print(rgb_name)

    #print(map_path + str(np.unique(map_img)))

    #grayscale are seperated by 20 shades just to make it easier to annotate
    map_img = map_img / 15

    #for each class index number found in the image
    for i in np.unique(map_img).tolist():

        #if the index has a label, use it, otherwise create a label
        if i in index_names_dict:
            label = index_names_dict[i]
        else:
            label = "index_%03i" % i

        #create binary mask of every pixel that has a value equal to the index
        mask_img = map_img == i

        #apply the mask to the color image
        rgb_masked_img = cv2.bitwise_and(rgb_img,rgb_img,mask=mask_img.astype(np.uint8))

        #join index label to the base output path to get a folder path for this image
        dir_path = os.path.join(base_output_path,label)

        #create the folder if it does not exist
        ensureDirPathExists(dir_path)

        #create a unique name for this image and join it the folder path
        rgb_masked_img_path = os.path.join(dir_path,"%s_%05i.png"%(rgb_name,count))

        #increment the counter used for naming images
        count += 1

        #write the masked image to file
        cv2.imwrite(rgb_masked_img_path,rgb_masked_img)
