#!/usr/bin/env python
#this is a ROS service node which takes an image and returns that image segmented
#The classes the image is segmented into are: background,baseball,monkey,pig,poo,poonicorn,fries and dice
import argparse
import os
import sys

import numpy as np

from ur5_picking_demo.srv import *
import rospy
import cv2
import roslib
from sensor_msgs.msg import Image
from std_msgs.msg import String
from cv_bridge import CvBridge, CvBridgeError
import parameters
from skimage import img_as_ubyte
import skimage.io as skio
# from model import resnet101 as resnet101 # slow: bicubic resize
class DepthCapturer:
    #this is the service itself, the code that runs every time the service is called
    def handle_inference(self,req):
        # Read image
        print "starting"
        bridge = CvBridge()
        print "getting image"
        img = rospy.wait_for_message('/camera/depth_registered/sw_registered/image_rect', sensor_msgs.msg.Image)
        print "got image"
        cv_image = bridge.imgmsg_to_cv2(img, desired_encoding="passthrough")
        print type(cv_image), cv_image.dtype
        filepath = 'urDepth_%i.png' % self.tracker
        skio.imsave(filepath, cv_image.astype(np.float16))
        self.tracker = self.tracker + 1
        print "done"
    #this is the code that only runs when the service node is first initialised in a terminal
    def DepthCapturer(self):
        self.tracker = 0
        rospy.init_node('depth_capturer')
        s = rospy.Service('capture', Capture, self.handle_inference)
        #don't call the service until "and now I spin" is printed in the terminal, otherwise it's not ready to be called yet (hasn't finished setting itself up)
        print ("finished setting up, ready for service requests")
        rospy.spin()

if __name__ == '__main__':
    server = DepthCapturer()
    server.DepthCapturer()
