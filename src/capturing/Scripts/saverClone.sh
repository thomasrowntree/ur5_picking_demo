#!/bin/bash
#source devel/setup.sh
#run image_view node only saving images when called to save from another terminal (this should be launched by camera.sh which will then call its)
rosrun image_view image_saver image:=/camera/rgb/image_rect_color _save_all_image:=false _filename_format:=urIMG%04i.png __name:=image_saver
