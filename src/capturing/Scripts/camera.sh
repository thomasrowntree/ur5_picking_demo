#!/bin/bash
#possible problems in using this script - screen library must be installed, paths to setup.bash and saverClone.sh must be correct
#source ~/catkin_ws/devel/setup.bash
# cd ~/catkin_ws/src/ur5_picking_demo/src/capturing
# #start another terminal which has a node ready to save pictures whenever asked
# #screen -S saverCloneScreen ~/catkin_ws/src/ur5_picking_demo/capturing/saverClone.sh
# #screen doesn't work so run that line above in another terminal
# echo "done"
# #cd ~/catkin_ws/src/ur5_picking_demo/capturing/Images
# ITERATION=1
# STR="SegmentedImages"
# #directory variable will be "UrImages1"
# DIR="$STR$ITERATION"
# #until directory variable does not equal an existing directory increase the number
# until [ ! -d "$DIR" ]; do
#   ITERATION=$[$ITERATION + 1]
# #  echo "$ITERATION"
#   DIR="$STR$ITERATION"
# done
# #make new directory
# mkdir "$DIR"
# cd $DIR
# NUM=1
# FOLDERSTR=""
STOP=false
#until button other than enter is pressed, take a photo whenever enter is pressed
while [ $STOP == false ]; do
  #mkdir "$FOLDER$NUM"
  #cd $NUM
  #wait for enter to be pressed
  read -n 1 -s  -r INPUT
  if [ INPUT == "" ]
  then
    STOP=true
  else
    rosservice call image_saver/save
    rosservice call capture
    #cd ..
    #NUM=$[$NUM + 1]
  fi
done
#close the terminal I started
#screen -S saverCloneScreen -X quit
