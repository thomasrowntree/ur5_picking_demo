# README #

### What it Does###
Sets up Ros such that whenever the enter key is pressed (while in the terminal which calls camera.sh) a picture is saved to a folder which is created called UrImages%i 
where %i is the largest number which didn't already have a directory with that name when the script was called


### How to Run ###

put both scripts in home/catkin_ws/src/ur5_picking_demo/capturing and run camera.sh from a terminal.

Requires having roscore running and anothe ros node posting image data to a topic.

you may have to edit file paths in the script if not running from ..../ur5_picking_demo

you may have to edit saverClone.sh if name of topic with image data does not match

roscore
rviz
roslaunch realsense_camera sr300_nodelet_rgbd.launch
mkdir [name you want]
cd [name you want]
rosrun image_view image_saver image:=/camera/rgb/image_rect_color _save_all_image:=false _filename_format:=urIMG%04i.png __name:=image_saver
cd ..
mkdir [name for depth photos]
cd [name for depth photos]
rosrun image_view image_saver image:=/camera/depth/depth_rect _save_all_image:=false _filename_format:=urDEPTH%04i.png __name:=image_depth_saver
rosservice call image_saver/save
cd ../scripts
./camera.sh
press enter whenever you want a photo taken
