#!/usr/bin/env python
import numpy as np
import math

#weightsPath = '/home/ur/catkin_ws/src/ur5_picking_demo/src/refinenet_pytorch/src/tmp/ep600_checkpoint.pth.tar'
weightsPath = '/home/ur/catkin_ws/src/ur5_picking_demo/src/refinenet_pytorch/src/checkpoints_dataset_20180208_2/ep285_checkpoint.pth.tar'
rgbTopic = 'camera/rgb/image_rect_color'

#Unordered dict of item data indexed by item name
item_data = {
'background':{'id':0,'colour':(0,0,0),'soft_percent':0,'hollow_percent':0,'touchable':False},
'hands':{'id':1,'colour':(111,222,12),'soft_percent':0,'hollow_percent':0,'touchable':False},
'poo':{'id':2,'colour':(33,67,101),'soft_percent':80,'hollow_percent':5,'touchable':True},
'chips':{'id':3,'colour':(0,0,255),'soft_percent':0,'hollow_percent':5,'touchable':True},
'monkey':{'id':4,'colour':(52,105,159),'soft_percent':80,'hollow_percent':10,'touchable':True},
'poonicorn':{'id':5,'colour':(0,255,0),'soft_percent':80,'hollow_percent':10,'touchable':True},
'pig':{'id':6,'colour':(203,192,255),'soft_percent':80,'hollow_percent':10,'touchable':True},
'ball':{'id':7,'colour':(255,0,0),'soft_percent':30,'hollow_percent':5,'touchable':True},
'die':{'id':8,'colour':(255,255,255),'soft_percent':0,'hollow_percent':5,'touchable':True},
'rope':{'id':9,'colour':(200,0,200),'soft_percent':0,'hollow_percent':90,'touchable':True},
'ring':{'id':10,'colour':(0,255,255),'soft_percent':0,'hollow_percent':95,'touchable':True},
'croc':{'id':11,'colour':(255,255,0),'soft_percent':0,'hollow_percent':5,'touchable':True},
'bb8':{'id':12,'colour':(255,165,0),'soft_percent':70,'hollow_percent':5,'touchable':True}
}

#Since the item_data is unordered, create some ordered lists
item_names = [""]*len(item_data)
label_colours = np.zeros([len(item_data),3],dtype=np.uint8)

#iterate through item_data and use the id to create ordered lists
for name, data in item_data.items():
    i = data["id"]
    item_names[i] = name
    label_colours[i] = data["colour"]

# originally, offset = 2.3 & spread = math.pi. I've made spread smaller to avoid tangles. Offset smaller actually makes it bigger
angle_offset = 2.19
radius = 0.75
spread = 2.8

for item in item_data:
    # to give even distribution of angles from 0 to spread, angle = item number * spread/(num items-1) - angle offset
    item_data[item]['angle'] = (item_data[item]['id']-2)*spread/(len(item_data)-3) - angle_offset
    # drop at constant radius from centre, changing the angle for each item:
    item_data[item]['drop_pos'] = (radius*math.cos(item_data[item]['angle']), radius*math.sin(item_data[item]['angle']))
