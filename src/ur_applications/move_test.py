#!/usr/bin/env python

# http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29

import rospy
import threading
import socket
import time
import math

from pyquaternion import Quaternion # pip install pyquaternion # http://kieranwynn.github.io/pyquaternion
import geometry_msgs.msg
import std_msgs.msg


def talker():
    move_pub = rospy.Publisher('move_to_pose', geometry_msgs.msg.Transform, queue_size=10)
    servo_pub = rospy.Publisher('servo_to_pose', geometry_msgs.msg.Transform, queue_size=10)
    speed_pub = rospy.Publisher('move_at_speed', geometry_msgs.msg.Twist, queue_size=10)
    gripper_pub = rospy.Publisher('gripper_pos', std_msgs.msg.Float32, queue_size=10)
    rospy.init_node('position_publisher_test', anonymous=True)


    t1 = createTransform(-0.4,-0.2,0.4,2.6,2.4,-2.3)
    t2 = createTransform(-0.4,-0.2,0.5,2.6,2.4,-2.3)



    while not rospy.is_shutdown():
        #move to start position
        move_pub.publish(createTransform(0.0,-0.55,0.4,0.0,math.pi,0.0))
        print("")
        print(t1)
        time.sleep(3)

        #close the gripper
        gripper_pub.publish(std_msgs.msg.Float32(100.0))
        time.sleep(1)

        #Servo in circle
        radius = 0.1
        for angle in range(0,360,5):
            x = 0 + radius*math.sin(angle/180.0*math.pi)
            y = -(0.55 - radius + radius*math.cos(angle/180.0*math.pi))
            print(createTransform(x,y,0.4,0.0,math.pi,0.0))
            servo_pub.publish(createTransform(x,y,0.4,0.0,math.pi,0.0))
            time.sleep(0.05)

        #servo to start position
        for angle in range(20):
            servo_pub.publish(createTransform(0.0,-0.55,0.4,0.0,math.pi,0.0))
            time.sleep(0.05)

        #open gripper
        gripper_pub.publish(std_msgs.msg.Float32(0.0))
        time.sleep(1)

        #speed
        speed_pub.publish(createTwist(0.1,0.0,0.0,0.0,0.0,0.0))
        time.sleep(1)
        speed_pub.publish(createTwist(0.0,0.1,0.0,0.0,0.0,0.0))
        time.sleep(1)
        speed_pub.publish(createTwist(0.0,0.0,0.1,0.0,0.0,0.0))
        time.sleep(1)
        speed_pub.publish(createTwist(0.0,0.0,0.0,0.2,0.0,0.0))
        time.sleep(1)
        speed_pub.publish(createTwist(0.0,0.0,0.0,0.0,0.3,0.0))
        time.sleep(1)
        speed_pub.publish(createTwist(0.0,0.0,0.0,0.0,0.0,0.4))
        time.sleep(1)





def createTwist(x,y,z,rx,ry,rz):
    linear = geometry_msgs.msg.Vector3(x,y,z)
    angular = geometry_msgs.msg.Vector3(rx,ry,rz)
    return geometry_msgs.msg.Twist(linear,angular)


def createTransform(x,y,z,rx,ry,rz):
    angle = math.sqrt( rx**2 + ry**2 + rz**2)
    q = Quaternion(axis=[rx,ry,rz],radians=angle)
    r = geometry_msgs.msg.Quaternion()
    r.w,r.x,r.y,r.z = q.elements


    trans = geometry_msgs.msg.Transform()
    trans.rotation = r
    trans.translation = geometry_msgs.msg.Vector3(x,y,z)
    return trans



if __name__ == '__main__':
    try:
        # help(Quaternion)
        # pose = geometry_msgs.msg.Pose()
        # help(pose.orientation)
        talker()
    except rospy.ROSInterruptException as e:
        print(e)
