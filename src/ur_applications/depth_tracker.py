#!/usr/bin/env python

# http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29

#import ur_rtde
import rospy
import threading
import time
import math
import pyquaternion  # pip install pyquaternion # http://kieranwynn.github.io/pyquaternion
import geometry_msgs.msg
import std_msgs.msg
import sensor_msgs.msg
import cv2
import cv_bridge
import numpy as np
import tf


class DepthTracker():

    def __init__(self):
        self.bridge = cv_bridge.CvBridge()
        self.move_loop_thread = threading.Thread(target=self.move_loop)
        self.target = (0,0,0)
        self.last_target_time = 0.0
        self.alive = False

    def __enter__(self):
        rospy.init_node("depth_tracker")
        rospy.Subscriber("/camera/depth/image_rect",sensor_msgs.msg.Image,self.image_callback)
        self.move_pub = rospy.Publisher('move_to_pose', geometry_msgs.msg.Transform, queue_size=10)
        self.servo_pub = rospy.Publisher('servo_to_pose', geometry_msgs.msg.Transform, queue_size=10)

        self.alive = True
        self.move_loop_thread.start()


    def __exit__(self,*args):
        self.alive = False
        self.move_loop_thread.join()

    def move_loop(self):
        tf_broadcaster = tf.TransformBroadcaster()
        tf_listener = tf.TransformListener()
        angle = -70.0
        height = 0.4
        radius = 0.3
        first_move = True



        time.sleep(0.5)

        #self.move_pub.publish(createTransform(0.0,-0.5,0.4,axis_angle[0],axis_angle[1],axis_angle[2]))

        while self.alive:

            try:
                tf_broadcaster.sendTransform((0,0,0),
                tf.transformations.quaternion_about_axis(math.radians(angle),(0,0,1)),
                rospy.Time.now(),"base_target","base")

                tf_broadcaster.sendTransform((0.108,-radius,height),
                tf.transformations.quaternion_about_axis(math.radians(-90),(0,0,1)),
                rospy.Time.now(),"camera_link_target","base_target")

                (trans, rot) = tf_listener.lookupTransform( 'camera_link','ur_tcp', rospy.Time())
                tf_broadcaster.sendTransform(trans,rot,rospy.Time.now(),"ur_tcp_target","camera_link_target")


                (trans, rot) = tf_listener.lookupTransform( 'ur_tcp_target','ur_tcp', rospy.Time())
                target_dist = math.sqrt(trans[0]**2 + trans[1]**2 + trans[2]**2)


                (target_trans,target_rot) = tf_listener.lookupTransform( 'base','ur_tcp_target', rospy.Time())

                trans = geometry_msgs.msg.Transform()
                trans.rotation = geometry_msgs.msg.Quaternion(*target_rot)
                trans.translation = geometry_msgs.msg.Vector3(*target_trans)


                #tf_broadcaster.sendTransform(traget_trans,target_rot,rospy.Time.now(),"test","base")
                if target_dist > 0.01 and first_move:
                    print("move")
                    self.move_pub.publish(trans)
                else:
                    first_move = False
                    self.servo_pub.publish(trans)
                    if time.time() - self.last_target_time < 0.5:
                        #print(self.target)
                        angle -= 3.0  * math.copysign(self.target[0]**2,self.target[0])
                        angle = np.clip(angle,-100,180)

                        height -= 0.02 * math.copysign(self.target[1]**2,self.target[1])
                        height = np.clip(height,0.25,0.8)

                        dist = self.target[2]
                        dist_target = 0.3
                        dist_error = dist-dist_target
                        radius += 0.5 * math.copysign(dist_error**2,dist_error)
                        radius = np.clip(radius,0.3,0.6)
                        pass


            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                pass


            time.sleep(0.05)

    def image_callback(self,data):

        img = self.bridge.imgmsg_to_cv2(data).copy()
        img[np.isnan(img)] = 5.0
        for i in range(2):
            #img = cv2.blur(img,(10,10))
            img = cv2.GaussianBlur(img, (9,9), 0)

        (minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(img)


        imgu8 = (img/5*255).astype(np.uint8)
        if minVal < 0.7:

            cv2.circle(imgu8, minLoc, 5, (255, 0, 0), 2)
            x,y = minLoc
            h,w = img.shape

            self.target =(x*2.0/w - 1.0,y*2.0/h - 1.0,minVal)
            self.last_target_time = time.time()
        cv2.imshow("depth",imgu8)
        cv2.waitKey(3)




def createTwist(x,y,z,rx,ry,rz):
    linear = geometry_msgs.msg.Vector3(x,y,z)
    angular = geometry_msgs.msg.Vector3(rx,ry,rz)
    return geometry_msgs.msg.Twist(linear,angular)


def createTransform(x,y,z,rx,ry,rz):
    angle = math.sqrt( rx**2 + ry**2 + rz**2)
    q = pyquaternion.Quaternion(axis=[rx,ry,rz],radians=angle)
    r = geometry_msgs.msg.Quaternion()
    r.w,r.x,r.y,r.z = q.elements


    trans = geometry_msgs.msg.Transform()
    trans.rotation = r
    trans.translation = geometry_msgs.msg.Vector3(x,y,z)
    return trans



if __name__ == '__main__':
    try:

        with DepthTracker():
            rospy.spin()

    except rospy.ROSInterruptException as e:
        print(e)
