#!/usr/bin/env python
import rospy
import cv2
import cv_bridge
import numpy as np
import ros_numpy #https://github.com/eric-wieser/ros_numpy
from PIL import Image
import math
import thinning
import random
import copy
#ROS message and services
import geometry_msgs.msg
import std_msgs.msg
import sensor_msgs.msg
import ur5_picking_demo.srv

from state_machine_utilities import State_Machine_Utilities
import parameters #added to the PYTHONPATH so that it could be imported


imageToPublish = None  ##this is actually a sensor_msgs.msg Image type, and will become an Image first time it is called


def estimateDepth(point_cloud_numpy,cx,cy):
    State_Machine_Utilities().timers.start("image function estimate depth")
    """This function returns the x,y and z values for a specified point in the point cloud, if the values are NaN it returns the estimate based off surrounding values"""
    i = 1
    x,y,z,rgb =  point_cloud_numpy[cy,cx]
    ##so now I check if the point i'm trying to grab has a depth of NaN, in which case I check the points next to it and average them
    while (math.isnan(x) and i < 11): ##11 is the number of times to look for non-NaN numbers before giving up
        smallArray = point_cloud_numpy['z'][cy-i:cy+i,cx-i:cx+i]
        numElements = np.prod(smallArray.shape)
        if (numElements > np.sum(np.isnan(smallArray))):  ##check that there are more elements than NaN's, so not all NaN
            maxVal = np.nanmax(smallArray)
            minVal = np.nanmin(smallArray)
            if (maxVal - minVal > minVal):
                #a big difference means that the estimate will not be accurate - safer to just not do anything
                print "big difference between highest and smallest depth"
                return float('nan'),float('nan'),float('nan')
            if (np.count_nonzero(~np.isnan(smallArray)) > 1): #if there are more than one non-NaN values found to average
                fourDimensionalSmallArray = point_cloud_numpy[cy-i:cy+i,cx-i:cx+i]
                x = np.nanmean(fourDimensionalSmallArray['x'])
                y = np.nanmean(fourDimensionalSmallArray['y'])
                z = np.nanmean(fourDimensionalSmallArray['z'])
                print "depth estimated from surrounding values"
                State_Machine_Utilities().timers.stop("image function estimate depth")
                return x,y,z

        i = i + 1

    if (i > 10):
        print "all NaN's near grab spot"
        return float('nan'),float('nan'),float('nan')
    return x,y,z

def setBlack():
    """Sets all the RVIZ screens to black"""
    bridge = cv_bridge.CvBridge()
    util = State_Machine_Utilities()

    img = np.zeros((480,640,3), np.uint8)
    rosCurrent = bridge.cv2_to_imgmsg(img.astype(np.uint8))
    util.overlay_pub.publish(rosCurrent)
    util.getPhoto_pub.publish(rosCurrent)
    util.findObjects_pub.publish(rosCurrent)
    util.findCentre_pub.publish(rosCurrent)


def publishRGB(bgr):
    """Publishes the image obtained from point cloud to one of the RVIZ screens"""
    bridge = cv_bridge.CvBridge()
    util = State_Machine_Utilities()
    global imageToPublish

    #Iimage needs to be in rgb
    cvrgb = bgr[:,:,::-1]
    rosrgb = bridge.cv2_to_imgmsg(cvrgb.astype(np.uint8))
    #set the latest version of the image as base image
    imageToPublish = cvrgb
    #publish to RVIZ
    util.overlay_pub.publish(rosrgb)
    util.getPhoto_pub.publish(rosrgb)

def publishSegment(segment):
    """publishes the mask returned by neural network to another RVIZ screen"""
    bridge = cv_bridge.CvBridge()
    util = State_Machine_Utilities()
    global imageToPublish

    #get latest version of image
    current = imageToPublish
    #add mask to it
    threeChannelResult = cv2.convertScaleAbs(parameters.label_colours[segment])
    current = cv2.addWeighted(current,0.5,threeChannelResult,0.5,0)
    #set latest version to include mask
    imageToPublish = current
    #publish latest version to RVIZ
    rosCurrent = bridge.cv2_to_imgmsg(current.astype(np.uint8))
    util.overlay_pub.publish(rosCurrent)
    util.findObjects_pub.publish(rosCurrent)

def publishCentre(cx,cy,rectPoints):
    """publishes the centrespoint it is grabbing and the rectangle it is aligning to"""
    bridge = cv_bridge.CvBridge()
    util = State_Machine_Utilities()
    global imageToPublish

    #get latest version of image
    current = copy.deepcopy(imageToPublish)
    #update it to include rectangle and grab point
    if ((rectPoints is None) == False):
        cv2.drawContours(current,[rectPoints],0,(0,0,255),2)
    cv2.circle(current,(cx,cy), 5, (255,255,255), -1)
    #set latest image to include rectangle and grab point
    #imageToPublish = current
    #publish it
    rosCurrent = bridge.cv2_to_imgmsg(current.astype(np.uint8))
    util.overlay_pub.publish(rosCurrent)
    util.findCentre_pub.publish(rosCurrent)



def notHollowPoint(dilated_mask,only_item_cloud):
    State_Machine_Utilities().timers.start("find solid object pick point")
    im2, contours, hierarchy = cv2.findContours(dilated_mask, 1, 2)
    biggest = 0
    #print(len(contours)),"is length of contours"

    for i in range(0,len(contours)):
        if (len(contours[i]) > len(contours[biggest])):
            biggest = i
    #first find the centre point of the object - this is the default grab location
    if (len(contours) != 0):
        if(len(contours[biggest]) > 15):
            cnt = contours[biggest]
            #image_functions.publishContour(cnt)
            rect = cv2.minAreaRect(cnt)
            rectPoints = cv2.boxPoints(rect)
            rectPoints = np.int0(rectPoints)
            # 'squishability' no longer exists
            #print parameters.item_data[item_to_pick_name]["squishable"]
            #print rectPoints
            cx = (rectPoints[0][0]+rectPoints[1][0]+rectPoints[2][0]+rectPoints[3][0])/4
            cy = (rectPoints[0][1]+rectPoints[1][1]+rectPoints[2][1]+rectPoints[3][1])/4
            #second, draw a box around the obejct and if all four corners are inside the image, then find sides to align to and grab the objet based off them
            if (0 < rectPoints[0][0] < 639 and 0 < rectPoints[1][0] < 639 and 0 < rectPoints[2][0] < 639 and 0 < rectPoints[3][0] < 639 and 0 < rectPoints[0][1] < 479
            and  0 < rectPoints[1][1] < 479 and  0 < rectPoints[2][1] < 479 and  0 < rectPoints[3][1] < 479):
                cornerOneX, cornerOneY, tempZ = estimateDepth(only_item_cloud,rectPoints[0][0],rectPoints[0][1])
                cornerTwoX, cornerTwoY, tempZ = estimateDepth(only_item_cloud,rectPoints[1][0],rectPoints[1][1])
                cornerThreeX, cornerThreeY, tempZ = estimateDepth(only_item_cloud,rectPoints[2][0],rectPoints[2][1])
                sideOne = math.sqrt(math.pow((cornerOneX - cornerTwoX),2) + math.pow((cornerOneY - cornerTwoY),2))
                sideTwo = math.sqrt(math.pow((cornerTwoX - cornerThreeX),2) + math.pow((cornerTwoY - cornerThreeY),2))
                gripBuffer = 35
                if (sideOne < sideTwo):
                    deltaY = (rectPoints[0][1] - rectPoints[1][1])
                    deltaX = (rectPoints[0][0] - rectPoints[1][0])
                    grip_width = max(100-100*sideOne/0.14-gripBuffer,0)
                    #print grip_width
                else:
                    deltaY = (rectPoints[1][1] - rectPoints[2][1])
                    deltaX = (rectPoints[1][0] - rectPoints[2][0])
                    grip_width = max(100-100*sideTwo/0.14-gripBuffer,0)
                    #print grip_width
                deltaY = float(deltaY)
                deltaX = float(deltaX)
                if (deltaX == 0):
                    angle = math.pi/2
                    #print "TRYING TO TURN 90 DEGREES BUT POSSIBLY FAILING MISERABLY" #this is printed to screen as I've never seen deltaX == 0 while testing so not sure if this works correctly
                    #I've seen above printed many times with no ill effects

                else:
                    print "side one is ", sideOne, "side Two is ", sideTwo
                    print deltaY, " and X is ", deltaX
                    #calculate angle based off change in x and change in y on the line robot is aligning to
                    angle = float(deltaY/deltaX)
                    print "angle starts at ",angle
                    if (math.isnan(angle)): #if angle is nan then tan^-1(y/x) = NaN thus y/x = NaN thus x = 0 or NaN thus angle = math,pi/2. this is covered earlier but a second failsafe is here too
                        angle = math.pi/2
                        print "angle due to NaN"
                    else:
                        angle = math.atan(angle)
                        print "delta y is ",(rectPoints[0][1] - rectPoints[1][1])
                        print "delta x is ",(rectPoints[0][0] - rectPoints[1][0])
                    angle = 0-angle
                if (angle < (0-math.pi/2)):
                    angle = angle + math.pi
                if (angle > math.pi/2):
                    angle = angle - math.pi
            else:
                ##this occurs if item is on edge of screen so alignment won't work properly
                angle = 0
                grip_width = 0
                print "box went outside screen view"
        else:
            #print item_to_pick_name + " is too small"
            return float('NaN'),float('NaN'),float('NaN'),float('NaN')
    else:
        #print"Cannot see " + item_to_pick_name + "anywhere"
        return float('NaN'),float('NaN'),float('NaN'),float('NaN')
    publishCentre(cx,cy,rectPoints)
    State_Machine_Utilities().timers.stop("find solid object pick point")
    return cx,cy,angle,grip_width

def hollowPoint():
    return float('NaN'),float('NaN'),float('NaN'),float('NaN')

def find_hollow_object_pick_point(segmentation_mask,image_for_annotation=None):
    """ Takes a binary segmenation mask where all pixels are 0 besides the mask
        of the object you want to pick.

        The algorithm will thin the mask down to a single line then pick
        a random point on the line and aligning the gripper with the angle of
        the line
    """
    State_Machine_Utilities().timers.start("find hollow object pick point")
    global imageToPublish
    image_for_annotation = imageToPublish
    #CONSTANTS
    angle_window_size = 5 #pixels. the number of pixels either side of the centre pixel used for getting line of best fit

    #use the thinning library to thin the mask down to single line
    thinned_mask = thinning.guo_hall_thinning(segmentation_mask)

    #remove any thinned lines that are too close to the edge
    border = angle_window_size+1
    thinned_mask[:,:border] = 0
    thinned_mask[:,-border:] = 0
    thinned_mask[:border,:] = 0
    thinned_mask[-border:,:] = 0

    #create a new 2D array of all the pixel coordinates
    pixels_coordinates = np.argwhere(thinned_mask)

    #get the number of pixels
    num_of_pixels,_ = pixels_coordinates.shape

    #TODO tom: return something meaningfull
    if num_of_pixels < 20:
        return float('NaN'),float('NaN'),float('NaN'),float('NaN')


    #choose a random pixel pick point
    random_index = random.randint(0,num_of_pixels-1)

    #get the coordinates of the random pixel
    cy,cx = pixels_coordinates[random_index]

    #use the angle_window_size to crop out a small region around the point
    cy1 = cy-angle_window_size
    cy2 = cy + angle_window_size + 1
    cx1 = cx-angle_window_size
    cx2 = cx + angle_window_size + 1
    line_region = thinned_mask[cy1:cy2, cx1:cx2]

    #get the coordinates of the pixel in the line
    line_points = np.argwhere(line_region)

    #fit a line to the pixels and get the x and y slopes
    line_params = cv2.fitLine(line_points, 2, 0, 0.01, 0.01)
    #get the slope or velocity of the line
    vcy = line_params[0,0]
    vcx = line_params[1,0]

    #if the line is pointing down, flip it so its pointing up
    if vcy > 0:
        vcy = - vcy
        vcx = - vcx

    #calculate the angle
    angle = -math.atan(vcx/vcy)

    #Draw a line on the image
    if image_for_annotation is not None:
        line_length = 40
        point1 = (int(cx), int(cy))
        point2 = (int(cx + vcx * line_length), int(cy + vcy * line_length))
        cv2.line(image_for_annotation,point1,point2,(255,0,0),2)

        #Draw a point on the pixel we want to pick from
        cv2.circle(image_for_annotation,(cx,cy),3,(0,0,0),-1)

        font = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = 0.5
        font_thickness = 1
        text = "%2.0f"%(angle/math.pi*180)
        (w,h),_ = cv2.getTextSize(text,font,font_scale,font_thickness)
        cv2.putText(image_for_annotation,text,(cx-w/2,cy+h+4), font, font_scale,(0,0,255),font_thickness,cv2.LINE_AA)
        publishCentre(cx,cy,None)

    grip_width = 0.0
    angle = 0 - angle
    State_Machine_Utilities().timers.stop("find hollow object pick point")
    return cx,cy,angle,grip_width
