import smach

class StateShutdown(smach.State):
	"""
	StateShutdown does nothing at the moment.
	"""

	def __init__(self):
		smach.State.__init__(self, outcomes = ['done'],
							input_keys = [],
							output_keys = [])

	def execute(self, userdata):

		print 'Shutting down...\n\n\n'

		return 'done'
