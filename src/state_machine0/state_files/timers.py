#!/usr/bin/env python
import time

class Timers():

    def __init__(self):
        self.clear_all()

    def start(self,timer_name):

        if not timer_name in self.timer_dict:
            timer = self.timer_dict[timer_name] = {"times":[]}
        else:
            timer = self.timer_dict[timer_name]

        timer["start_time"] = time.time()
        timer["started"] = True


    def stop(self,timer_name):
        if timer_name in self.timer_dict:
            timer = self.timer_dict[timer_name]
            if(timer["started"]):
                timer["times"].append(time.time() - timer["start_time"])
            timer["started"] = False


    def clear(self,timer_name):
        if timer_name in self.timer_dict:
            del self.timer_dict[timer_name]

    def clear_all(self):
        self.timer_dict = {}

    def __str__(self):
        timer_names = self.timer_dict.keys()
        if len(timer_names) >0:
            timer_names.sort()

            max_name_length = 0
            for timer_name in timer_names:
                max_name_length = max(max_name_length,len(timer_name))
            str_format = "%%%is: %%6.2f ms %%4i\n" % max_name_length

            return_str = ""
            for timer_name in timer_names:
                timer = self.timer_dict[timer_name]
                count = len(timer["times"])
                if count > 0:
                    total_time = sum(timer["times"])

                    average_time = total_time / count * 1000.0 #ms

                    return_str += str_format % (timer_name,average_time,count)


            return return_str
        return "No timers"

if __name__ == "__main__":
    t = Timers()
    print(t)
    for i in range(10):
        t.start("one")
        time.sleep(0.1)
        t.stop("one")

    for i in range(10):
        t.start("twoasdf")
        time.sleep(0.01)
        t.stop("twoasdf")

    print(t)
