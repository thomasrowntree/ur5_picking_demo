import smach
import rospy
import math
import std_msgs.msg
from state_machine_utilities import State_Machine_Utilities, RobotAtTransformTimeoutException
import tf
import state_base


class StateHand(state_base.StateBase):
	"""
	When StateProcessImage detects a hand in the frame, this state shakes the robot's 'head'
	and returns to StateProcessImage.
	"""

	def __init__(self):
		smach.State.__init__(self, outcomes = ['success', 'move_failed', 'paused'],
							input_keys = [],
							output_keys = ['current_state'])
		self.tf_broadcaster = tf.TransformBroadcaster()
		self.tf_listener = tf.TransformListener()


	def tryExecute(self, userdata):
		userdata.current_state = 'hand'
		util = State_Machine_Utilities()
		print 'Hand detected'

		# shake head pi/8 clockwise:
		(trans, rot) = self.tf_listener.lookupTransform( 'base','ur_tcp', rospy.Time())
		q_shake = tf.transformations.quaternion_about_axis(math.pi/8, (0,0,1))
		q_total = tf.transformations.quaternion_multiply(rot,q_shake)
		util.moveTcpToTransform(trans, q_total)

		# wait until robot has moved:
		try:
			util.waitUntilRobotIsAtTransform(trans, q_total, timeoutTime=6)
		except RobotAtTransformTimeoutException:
			return 'move_failed'

		# shake head pi/4 anticlockwise:
		(trans, rot) = self.tf_listener.lookupTransform( 'base','ur_tcp', rospy.Time())
		q_shake = tf.transformations.quaternion_about_axis(-math.pi/4, (0,0,1))
		q_total = tf.transformations.quaternion_multiply(rot,q_shake)
		util.moveTcpToTransform(trans, q_total)

		# wait until robot has moved:
		try:
			util.waitUntilRobotIsAtTransform(trans, q_total, timeoutTime=6)
		except RobotAtTransformTimeoutException:
			return 'move_failed'

		return 'success'
