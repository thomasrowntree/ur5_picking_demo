import smach
import rospy
import random
import parameters
from state_machine_utilities import State_Machine_Utilities, RobotAtTransformTimeoutException
import state_base
import tf


class StateInitialise(state_base.StateBase):
	"""
	This state is executed only when the state machine starts up, and when a move_fail is returned
	(presumably meaning the robot has stopeed.) It randomises the list of the touchable paramaters,
	waits for manual input, and tests that the robot can move.
	"""

	def __init__(self):
		smach.State.__init__(self, outcomes = ['success', 'paused'],
							input_keys = [],
							output_keys = ['current_state', 'item_list'])
		self.tf_broadcaster = tf.TransformBroadcaster()
		self.tf_listener = tf.TransformListener()


	def tryExecute(self, userdata):
		userdata.current_state = 'initialise'
		util = State_Machine_Utilities()


		# make random list of all touchable objects from parameters:
		local_item_list = []
		for item in parameters.item_data:
			if (parameters.item_data[item]['touchable']):
				local_item_list.append(item)
		random.shuffle(local_item_list)

		userdata.item_list = local_item_list


		# wait for manual input:
		print 'Initialising state machine. Check that the robot is not tangled.'
		user_input = raw_input('Press enter to continue or ctrl+c to cancel: ')


		while True:
		# move the robot arm to just above capturing position:
			test_t, test_r = util.createTransform(0.22690,-0.18126,0.57172,1.1475,-2.8555,-0.279)
			util.moveTcpToTransform(test_t, test_r)

			# check the robot did move:
			try:
				util.waitUntilRobotIsAtTransform(test_t, test_r, timeoutTime=4)
				print 'Passed test one'
				break
			except RobotAtTransformTimeoutException:
				print 'Robot cannot move. Ensure it switched on.'


		while True:
			# move the robot arm to capturing position:
			capture_pos_t, capture_pos_r = util.createTransform(0.22690,-0.18126,0.55172,1.1475,-2.8555,-0.279)
			util.moveTcpToTransform(capture_pos_t, capture_pos_r)

			# check the robot did move:
			try:
				util.waitUntilRobotIsAtTransform(capture_pos_t, capture_pos_r, timeoutTime=2)
				print 'Passed test two'
				print 'Initialisation complete'
				return 'success'
			except RobotAtTransformTimeoutException:
				print 'Robot cannot move. Ensure it is switched on.'
