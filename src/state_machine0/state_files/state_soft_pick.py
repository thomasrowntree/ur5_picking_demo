import smach
import rospy
import time
import std_msgs.msg
from state_machine_utilities import State_Machine_Utilities, RobotAtTransformTimeoutException
import state_base


class StateSoftPick(state_base.StateBase):
	"""
	This state attempts to pick up one of the soft toys, once the robot has moved to just above it.
	"""

	def __init__(self):
		smach.State.__init__(self, outcomes = ['success','move_failed', 'grip_failed', 'paused'],
							input_keys = ['item_to_pick_name','pick_pose'],
							output_keys = ['current_state'])


	def tryExecute(self, userdata):
		userdata.current_state = 'soft_pick'
		util = State_Machine_Utilities()

		# half close the gripper ready to push into object:
		util.gripper_pub.publish(std_msgs.msg.Float32(65.0))
		time.sleep(0.5)

		# get the pick pose from the userdata that should have been passed from the image processing state:
		t_pick,r_pick = userdata.pick_pose

		# check z is at least 5mm above the table level to avoid crashing:
		x,y,z = t_pick
		#t_pick = (x,y,max(z-0.01,0.005))
		t_pick = (x,y,max(z,0.005))

		t_tcp_target,r_tcp_target = util.moveGripperToTransform(t_pick,r_pick)

		# wait until robot has moved:
		try:
			util.waitUntilRobotIsAtTransform(t_tcp_target,r_tcp_target, timeoutTime=6)
		except RobotAtTransformTimeoutException:
			return 'move_failed'

		# fully close the gripper:
		util.gripper_pub.publish(std_msgs.msg.Float32(100.0))
		time.sleep(0.8)

		return 'success' #DEBUG tom: skip object detected step as it keeps failing

		# # check whether gripper actually grabbed object:
		# object_grabbed = rospy.wait_for_message('gripper_obj_detected', std_msgs.msg.Bool)
		# if object_grabbed.data:
		# 	return 'success'
		# else:
		# 	# half-open the gripper to let go in case object was half-grabbed:
		# 	util.gripper_pub.publish(std_msgs.msg.Float32(65.0))
		# 	time.sleep(0.5)
		# 	return 'grip_failed'
