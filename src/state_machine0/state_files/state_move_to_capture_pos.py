import smach
import rospy
import time
import std_msgs.msg
from state_machine_utilities import State_Machine_Utilities, RobotAtTransformTimeoutException
import state_base


class StateMoveToCapturePos(state_base.StateBase):
	"""
	This state moves to the capturing position, ready to take a photo.
	"""

	def __init__(self):
		smach.State.__init__(self, outcomes = ['success', 'move_failed', 'paused'],
							input_keys = [],
							output_keys = ['current_state'])

	def tryExecute(self, userdata):
		userdata.current_state = 'move_to_capture'
		util = State_Machine_Utilities()


		# move the robot arm to capturing position:
		capture_pos_t, capture_pos_r = util.createTransform(0.22690,-0.18126,0.55172,1.1475,-2.8555,-0.279)
		util.moveTcpToTransform(capture_pos_t, capture_pos_r)

		# wait until robot has moved:
		try:
			util.waitUntilRobotIsAtTransform(capture_pos_t, capture_pos_r, timeoutTime=10)
		except RobotAtTransformTimeoutException:
			return 'move_failed'

		# open the gripper:
		util.gripper_pub.publish(std_msgs.msg.Float32(0.0))
		time.sleep(0.5)

		return 'success'
