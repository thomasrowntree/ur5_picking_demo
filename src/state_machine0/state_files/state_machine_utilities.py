#!/usr/bin/env python
import rospy
import time
import math
import tf
from pyquaternion import Quaternion # pip install pyquaternion # http://kieranwynn.github.io/pyquaternion
import geometry_msgs.msg
import std_msgs.msg
import parameters
from sensor_msgs.msg import Image
import timers


class State_Machine_Utilities:
    """
    This is just a class you pass through to control the Utilities_Hidden class,
    which is shared between all State_Machine_Utilities classes.
    """

    # storage for the instance reference:
    __instance = None


    def __init__(self):
        # check whether we already have an instance:
        if State_Machine_Utilities.__instance is None:
            # Create and remember instance
            State_Machine_Utilities.__instance = State_Machine_Utilities.Utilities_Hidden()

        # Store instance reference as the only member in the handle
        self.__dict__['_State_Machine_Utilities__instance'] = State_Machine_Utilities.__instance

    def __getattr__(self, attr):
        # delegate access to implementation
        return getattr(self.__instance, attr)

    def __setattr__(self, attr, value):
        # delegate access to implementation
        return setattr(self.__instance, attr, value)


    class Utilities_Hidden:

        def __init__(self):
            rospy.init_node('state_machine_utilities', anonymous=True, disable_signals=True)
            self.move_pub = rospy.Publisher('move_to_pose', geometry_msgs.msg.Transform, queue_size=10)
            self.servo_pub = rospy.Publisher('servo_to_pose', geometry_msgs.msg.Transform, queue_size=10)
            self.gripper_pub = rospy.Publisher('move_gripper_to_pos', std_msgs.msg.Float32, queue_size=10)
            self.speed_pub = rospy.Publisher('move_at_speed', geometry_msgs.msg.Twist, queue_size=10)
            self.tf_broadcaster = tf.TransformBroadcaster()
            self.tf_listener = tf.TransformListener()
            self.overlay_pub = rospy.Publisher('overlay', Image, queue_size=10)
            self.getPhoto_pub = rospy.Publisher('getPhoto', Image, queue_size=10)
            self.findObjects_pub = rospy.Publisher('findObjects', Image, queue_size=10)
            self.findCentre_pub = rospy.Publisher('findCentre', Image, queue_size=10)

            self.timers = timers.Timers()


        def createTwist(self,x,y,z,rx,ry,rz):
            linear = geometry_msgs.msg.Vector3(x,y,z)
            angular = geometry_msgs.msg.Vector3(rx,ry,rz)
            return geometry_msgs.msg.Twist(linear,angular)


        def createTransform(self,x,y,z,rx,ry,rz):
            angle = math.sqrt( rx**2 + ry**2 + rz**2)
            r = (0,0,0,1)
            if angle > 0:
                q = Quaternion(axis=[rx,ry,rz],radians=angle)
                rw,rx,ry,rz = q.elements
                r = (rx,ry,rz,rw)

            t = (x,y,z)


            # trans = geometry_msgs.msg.Transform()
            # trans.rotation = r
            # trans.translation = geometry_msgs.msg.Vector3(x,y,z)
            # # return trans

            #t and r should be able to be directly passed to moveGripperTo Transofrm now
            return t,r


        def multiplyTransforms(self,t1=(0,0,0),r1=(0,0,0,1),t2=(0,0,0),r2=(0,0,0,1)):
            #ROS x,y,z,w
            #pyquaternion w,x,y,z
            q1 = Quaternion(r1[3],r1[0],r1[1],r1[2])
            q2 = Quaternion(r2[3],r2[0],r2[1],r2[2])

            #rotate the second transtaion by the first quaternion
            t2_ = q1.rotate(t2)

            #add the first translation to the rotated second translation
            t3 = (t1[0]+t2_[0], t1[1]+t2_[1], t1[2]+t2_[2])

            #multiply quaternions
            q3 = q1 * q2

            #change the order so that it is in ros quaternion format
            r3 = (q3[1],q3[2],q3[3],q3[0])

            return t3,r3


        def moveTcpToTransform(self, trans, rot):

            new_transform = geometry_msgs.msg.Transform()
            new_transform.translation = geometry_msgs.msg.Vector3(*trans)
            new_transform.rotation = geometry_msgs.msg.Quaternion(*rot)
            #return new_transform
            self.move_pub.publish(new_transform)

        def moveGripperToTransform(self,trans,rot):
            #We want to find out the tcp pose if we put the gripper point at the pick_pose
            #look up the transform of the tcp with respect to the griper point
            (t_gripper_to_tcp, r_gripper_to_tcp) = self.lookupTransform( 'gripper_point','ur_tcp', rospy.Time())
            t_tcp,r_tcp = self.multiplyTransforms(trans,rot,t_gripper_to_tcp,r_gripper_to_tcp)
            self.moveTcpToTransform(t_tcp,r_tcp)
            return t_tcp,r_tcp


        def isRobotAtTransform(self, target_trans, target_rot, position_tolerance=0.001, rotation_tolerance=0.01):
            """
            target_transform should be geometry_msgs.msg.Transform
            """
            #try and lookup the tranform from the base to the ur_tcp
            try:
                (current_trans, current_rot) = self.tf_listener.lookupTransform( 'base','ur_tcp', rospy.Time())
            except tf.LookupException as e:
                #if we can't lookup the transform then assume we are not at the transform and return false
                return False

            #instanciate quaternion objects
            current_q = Quaternion(q1=current_rot[3], q2=current_rot[0], q3=current_rot[1], q4=current_rot[2])
            target_q = Quaternion(q1=target_rot[3], q2=target_rot[0], q3=target_rot[1], q4=target_rot[2])

            #calculate geodesic distance (arc length) bwetween the two quaternions.
            #http://kieranwynn.github.io/pyquaternion/#distance-computation
            quaternion_dist = Quaternion.distance(current_q, target_q)
            quaternion_dist_neg = Quaternion.distance(-current_q, target_q)

            #Just use pythagoras to calculate translation distance
            position_dist = math.sqrt((target_trans[0]-current_trans[0])**2 + (target_trans[1]-current_trans[1])**2 + (target_trans[2]-current_trans[2])**2)
            #print(quaternion_dist, quaternion_dist_neg, position_dist)
            #if both the quaternion and position distances are less than the tolerance return true; otherwise, return false
            return (quaternion_dist < rotation_tolerance or quaternion_dist_neg < rotation_tolerance) and position_dist < position_tolerance


        def waitUntilRobotIsAtTransform(self, target_trans, target_rot, position_tolerance=0.001, rotation_tolerance=0.01, timeoutTime=10):
            #take note of the start time for time out:
            start_time = time.time()
            #while not timed out:
            while time.time() - start_time < timeoutTime:

                #check if the robot is at the target_transform:
                if self.isRobotAtTransform(target_trans, target_rot, position_tolerance, rotation_tolerance):
                    # print 'hecc it worked'
                    return True
                #sleep a little to rest the cpu:
                rospy.sleep(0.02)

            raise RobotAtTransformTimeoutException("The Robot was never at the target transform before the time out occured")


        def lookupTransform(self,frame_from,frame_to,lookup_time,timeoutTime=0.5):
            #take note of the start time for time out
            start_time = time.time()
            #while not timed out
            while True:
                try:
                    trans = self.tf_listener.lookupTransform(frame_from, frame_to, rospy.Time())
                    return trans
                except (tf.LookupException, tf.ExtrapolationException):
                    if time.time() - start_time > timeoutTime:
                        raise
                rospy.sleep(0.1)


class RobotAtTransformTimeoutException(Exception):
    pass



# if __name__ == "__main__":
#     util = State_Machine_Utilities()
#     r1 = tf.transformations.quaternion_about_axis(math.pi/2.0,(0,0,-1))
#     tr3 = util.multiplyTransforms((1,0,0),r1,(2,0,0),(0,0,0,1))
#     print(tr3)
