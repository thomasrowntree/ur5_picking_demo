import smach


class StateBase(smach.State):
	"""
	This is the base state from which other states should inherit
	so that the state machine can be paused at any time (rather than every
	state containing a try/except statement).
	Other states should define tryExecute() rather than execute() methods
	in order to be able to pause. Ignore this for states that do not need 
	pause functionality (StatePause and StateShutdown etc).
	"""
	
	def execute(self, userdata):
		try:
			return self.tryExecute(userdata)

		except KeyboardInterrupt:
			return 'paused'
