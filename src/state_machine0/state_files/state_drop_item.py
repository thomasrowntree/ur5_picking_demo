import smach
import rospy
import time
import math
import std_msgs.msg
from state_machine_utilities import State_Machine_Utilities, RobotAtTransformTimeoutException
import tf
import state_base
import parameters


class StateDropItem(state_base.StateBase):
	"""
	Once an object has been grabbed, this state moves and drops it at its unique spot around the table.
	"""

	def __init__(self):
		smach.State.__init__(self, outcomes = ['success', 'move_failed', 'grip_failed', 'paused'],
							input_keys = ['item_to_pick_name'],
							output_keys = ['current_state'])
		#create ros publisher for move to pose topic
		self.tf_broadcaster = tf.TransformBroadcaster()
		self.tf_listener = tf.TransformListener()


	def tryExecute(self, userdata):
		userdata.current_state = 'drop_item'
		util = State_Machine_Utilities()


		# move arm up 10cm to avoid bumping into other stuff:
		(trans, rot) = self.tf_listener.lookupTransform( 'base','ur_tcp', rospy.Time())
		(x,y,z) = trans
		trans = (x, y, z+0.1)
		util.moveTcpToTransform(trans, rot)

		# wait until robot has moved:
		try:
			util.waitUntilRobotIsAtTransform(trans, rot, timeoutTime=4)
		except RobotAtTransformTimeoutException:
			return 'move_failed'


		# # check whether gripper is still grabbing object; if not, save in variable object_grabbed:
		# object_grabbed = rospy.wait_for_message('gripper_obj_detected', std_msgs.msg.Bool)


		# rotate through y to get in gripper reference frame:
		q1 = tf.transformations.quaternion_about_axis(math.pi,(0,1,0))

		# take the object's unique angle from parameters,
		# make negative and subtract pi/2 to align reference frames & fix offset:
		placement_gripper_angle =  -parameters.item_data[userdata.item_to_pick_name]['angle'] - math.pi/2
		q2 = tf.transformations.quaternion_about_axis(placement_gripper_angle,(0,0,1))
		q3 = tf.transformations.quaternion_multiply(q1,q2)

		# obtain object's x,y coordinates from parameters, and append height (z) of 50cm:
		drop_pos_trans_position = parameters.item_data[userdata.item_to_pick_name]['drop_pos'] + (0.5,)
		self.tf_broadcaster.sendTransform(drop_pos_trans_position, q3, rospy.Time.now(), 'drop_point', 'base')

		# move above dropping position:
		util.moveTcpToTransform(drop_pos_trans_position, q3)

		# wait until robot has moved:
		try:
			util.waitUntilRobotIsAtTransform(drop_pos_trans_position, q3, timeoutTime=6)
		except RobotAtTransformTimeoutException:
			return 'move_failed'


		#DEBUG tom: dont check for failed grip because it keeps failing
		# # if gripper was not grabbing before, check again here:
		# if not object_grabbed.data:
		# 	object_grabbed = rospy.wait_for_message('gripper_obj_detected', std_msgs.msg.Bool)
		# 	if not object_grabbed.data:
		# 		# if gripper reported nothing both times, it's probably trustworthy, so assume the object has fallen and return failed:
		# 		return 'grip_failed'


		# rewrite x,y,z coordinates to go lower, to a drop height of 32cm:
		drop_pos_trans_position = parameters.item_data[userdata.item_to_pick_name]['drop_pos'] + (0.32,)

		# move down to the dropping position:
		util.moveTcpToTransform(drop_pos_trans_position, q3)

		# wait until robot has moved:
		try:
			util.waitUntilRobotIsAtTransform(drop_pos_trans_position, q3, timeoutTime=4)
		except RobotAtTransformTimeoutException:
			return 'move_failed'

		# release the gripper:
		util.gripper_pub.publish(std_msgs.msg.Float32(0.0))
		rospy.sleep(1.0)

		# rewrite x,y,z coordinates to go up again:
		drop_pos_trans_position = parameters.item_data[userdata.item_to_pick_name]['drop_pos'] + (0.5,)

		#move up from the dropping position:
		util.moveTcpToTransform(drop_pos_trans_position, q3)

		# wait until robot has moved:
		try:
			util.waitUntilRobotIsAtTransform(drop_pos_trans_position, q3, timeoutTime=4)
		except RobotAtTransformTimeoutException:
			return 'move_failed'


		return 'success'
