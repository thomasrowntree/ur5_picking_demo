import smach
import rospy
import time
import random
import std_msgs.msg
from state_machine_utilities import State_Machine_Utilities, RobotAtTransformTimeoutException
import state_base
import parameters #added to the PYTHONPATH so that it could be imported


class StateMoveAboveObject(state_base.StateBase):
	"""
	Once an object has been etected in StateProcessImage, this state moves to a position
	just above that object, ready to pick it up.
	"""

	def __init__(self):
		smach.State.__init__(self, outcomes = ['soft_success', 'hard_success', 'move_failed', 'paused'],
							input_keys = ['item_to_pick_name','pick_pose'],
							output_keys = ['current_state'])


	def tryExecute(self, userdata):
		userdata.current_state = 'soft_pick'
		util = State_Machine_Utilities()

		#get the pick pose from the userdata that should have been passed from the image processing state
		t_pick,r_pick = userdata.pick_pose

		# move the gripper to 5cm above object to be picked, and ensure this is at least 5mm above table:
		x,y,z = t_pick
		t_pick = (x,y,max(z+0.05, 0.005))
		t_tcp_target,r_tcp_target = util.moveGripperToTransform(t_pick,r_pick)

		# wait until robot has moved:
		try:
			util.waitUntilRobotIsAtTransform(t_tcp_target,r_tcp_target, timeoutTime=6)
		except RobotAtTransformTimeoutException:
			return 'move_failed'

		# go to random picking state weighted by object's soft percentage:
		rand_int = random.randint(0,100)
		print rand_int, parameters.item_data[userdata.item_to_pick_name]['soft_percent']
		if rand_int < parameters.item_data[userdata.item_to_pick_name]['soft_percent']:
			print 'Soft selected'
			return 'soft_success'
		else:
			print 'Hard selected'
			return 'hard_success'
