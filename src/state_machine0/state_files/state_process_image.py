#!/usr/bin/env python

"""This state takes the image from the camera, passes it through the neural network, and then detects objects based on the mask returned by the network.

     It also determines where and how best to grab the item detected"""

import smach
import rospy
import cv2
import cv_bridge
import numpy as np
import ros_numpy #https://github.com/eric-wieser/ros_numpy
import numpy.lib.recfunctions
from PIL import Image
import tf
import time
import math
import random
import pyquaternion
import operator

#ROS message and services
import geometry_msgs.msg
import std_msgs.msg
import sensor_msgs.msg
import ur5_picking_demo.srv

import parameters #added to the PYTHONPATH so that it could be imported
import image_functions

import state_base

from state_machine_utilities import State_Machine_Utilities

# define Process Image state:
class StateProcessImage(state_base.StateBase):

    def __init__(self): #set up the variables needed for the state
        smach.State.__init__(self, outcomes = ['success', 'paused', 'cant_see_item', 'hand_detected'],
                            input_keys = ['item_list', 'grip_width'],
                            output_keys = ['item_list', 'item_to_pick', 'pick_pose', 'current_state', 'grip_width'])

        self.image_segmentation_service = rospy.ServiceProxy('inference',ur5_picking_demo.srv.Inference)
        self.point_cloud_pub = rospy.Publisher('segmented_point_cloud', sensor_msgs.msg.PointCloud2, queue_size=1)
        self.transform_broadcaster = tf.TransformBroadcaster()
    def tryExecute(self, userdata):

        userdata.current_state = 'process_image'

        #get util object. Note that the ros node is initiated in here
        util = State_Machine_Utilities()
        bridge = cv_bridge.CvBridge()

        util.timers.start("total_state_process_images")
        image_functions.setBlack()
        print("Waiting for image and point clound messages")
        #wait for the point cloud from the real sense
        point_cloud_msg = rospy.wait_for_message('/camera/depth_registered/points', sensor_msgs.msg.PointCloud2)
        print("I have point cloud")

        #convert point cloud message to numpy field array
        util.timers.start("point cloud to image")
        point_cloud_numpy = ros_numpy.numpify(point_cloud_msg)

        #extract z values for later reference:
        z_pointcloud = point_cloud_numpy['z'].copy()

        rgbImg = ros_numpy.point_cloud2.split_rgb_field(point_cloud_numpy)
        #get image from RGB value in point cloud
        rgbImg = np.lib.recfunctions.drop_fields(rgbImg,('x','y','z'))
        rgbImg = Image.fromarray(rgbImg, 'RGB')
        rgbImg = numpy.array(rgbImg)
        util.timers.stop("point cloud to image")
        image_msg = bridge.cv2_to_imgmsg(rgbImg.astype(np.uint8))
        #publish image to screen to show image obtained
        image_functions.publishRGB(rgbImg)

        util.timers.start("image segmentation service")
        #send image to the segmentation service to get a map back
        segmentation_msg = self.image_segmentation_service(image_msg)
        util.timers.stop("image segmentation service")

        #convert ros image to grayscale numpy image
        segmentation_map = bridge.imgmsg_to_cv2(segmentation_msg.mask, desired_encoding="passthrough")
        image_functions.publishSegment(segmentation_map)


        #show the map
        # cv2.imshow("map",segmentation_map*20)
        # cv2.waitKey(1000)

        # check if the image comprises > 2% hand:
        hand_id = parameters.item_data["hands"]["id"]
        #find upper bound for num pixels to make 2% of total image:
        max_percent = 0.02
        x,y = segmentation_map.shape
        limit = max_percent*x*y
        if np.sum(segmentation_map==hand_id)>limit:
            return 'hand_detected'


        util.timers.start("find highest item")
        # we need to remove nans from z_pointcloud as they mess up the nanargmin instruction.
        # this bool array is True for all NaNs:
        z_pointcloud_bool = np.isnan(z_pointcloud)

        # compute the average height ignoring NaNs; all nans will be set to this
        nan_mean = np.nanmean(z_pointcloud)

        # set all NaNs to the mean value:
        z_pointcloud[z_pointcloud_bool] = nan_mean

        # blur z_pointcloud to reduce random speckles and edges:
        z_pointcloud = cv2.blur(z_pointcloud,(3,3))

        # find index of minimum z element in pointcloud (closest to the camera):
        yVal, xVal = np.unravel_index(np.nanargmin(z_pointcloud), z_pointcloud.shape)

        # find which item is at min index in segmentation map:
        highest_item_id = segmentation_map[yVal][xVal]
        # now we have an id, need to get name of item:
        highest_item_name = parameters.item_names[highest_item_id]
        print '\n\n********************HIGHEST ITEM IS ', highest_item_name

        # Now IF this is at the bottom of the list, it might mean that we've just tried to grab it unsuccessfully.
        # To avoid an endless loop, in this case, we'll find the second highest item instead.
        if userdata.item_list[-1] == highest_item_name:

            #set all of the FIRST highest item to 1 (as 1m is big enough that it's never going to be min)
            z_pointcloud[segmentation_map == highest_item_id] = 1
            # find index of minimum z element in pointcloud (closest to the camera):
            yVal, xVal = np.unravel_index(np.nanargmin(z_pointcloud), z_pointcloud.shape)

            # find which item is at min index in segmentation map:
            highest_item_id = segmentation_map[yVal][xVal]
            # now we have an id, need to get name of item:
            highest_item_name = parameters.item_names[highest_item_id]
            print '\n********************SECOND ITEM IS ', highest_item_name


        # if this is a legit item, move to beginning of userdata.item_list:
        if parameters.item_data[highest_item_name]['touchable']:
            userdata.item_list.remove(highest_item_name)
            userdata.item_list = [highest_item_name] + userdata.item_list

        util.timers.stop("find highest item")

        # now go through the newly-ordered list, hopefully finding the first (highest) item first:
        found_item = False

        util.timers.start("find either pick point")
        for name in userdata.item_list:
            only_item_cloud = point_cloud_numpy.copy()
            item_to_pick_name = name

            #get the grayscale index of the item we want to pick
            item_to_pick_id = parameters.item_data[item_to_pick_name]['id']

            #get binary mask of itempoint_cloud
            item_to_pick_mask = segmentation_map == item_to_pick_id

            item_to_pick_mask_inv =  item_to_pick_mask == False
            item_to_pick_mask = np.array(item_to_pick_mask, dtype=np.uint8)
            #item_to_pick_mask_inv = np.array(item_to_pick_mask_inv, dtype=np.uint8)
            kernel = np.ones((5,5),np.uint8)
            dilated_mask = cv2.erode(item_to_pick_mask,kernel,iterations=5)

            randomNum = random.randint(1,100)
            if (randomNum <= parameters.item_data[item_to_pick_name]['hollow_percent']):
                cx,cy,angle,userdata.grip_width = image_functions.find_hollow_object_pick_point(item_to_pick_mask)
                if (math.isnan(cx)):
                    cx,cy,angle,userdata.grip_width = image_functions.notHollowPoint(dilated_mask,only_item_cloud)
            else:
                cx,cy,angle,userdata.grip_width = image_functions.notHollowPoint(dilated_mask,only_item_cloud)
                if (math.isnan(cx)):
                    cx,cy,angle,userdata.grip_width = image_functions.find_hollow_object_pick_point(item_to_pick_mask)
            if (math.isnan(cx)):
                print "both methods returned NaN"
                continue
            dilated_mask = np.array(dilated_mask, dtype=bool)
            dilated_mask = dilated_mask == False


            #use the inverse segmentation to set all points that are not on the item to nan
            only_item_cloud[dilated_mask] = (np.nan,np.nan,np.nan,np.nan)
            x,y,z = image_functions.estimateDepth(only_item_cloud,cx,cy)
            if (math.isnan(x)):
                print"all NaN around centre of " + item_to_pick_name
                #image_functions.publaiming forishCentre(cx,cy)
                continue
            print 'found ' + item_to_pick_name
            found_item = True
            #print x,y,z
            break

        util.timers.stop("find either pick point")


        if found_item:
            # move found item to bottom of list:
            userdata.item_list.remove(item_to_pick_name)
            userdata.item_list.append(item_to_pick_name)
        else:
            print "Couldn't see any items"
            time.sleep(0.5)
            return "cant_see_item"


        #range limit the angle so that the camera mount doesn't bash into the arm

        angle = np.clip(angle,math.radians(-60.0),math.radians(60.0))

        #TODO tom: use util.multiplyTransforms()

        #get the transform for the point cloud frame with respect to the base
        (t_camera, r_camera) = util.lookupTransform( 'base','camera_rgb_optical_frame', rospy.Time())
        angleRotate = tf.transformations.quaternion_about_axis(angle,(0,0,1))

        #angleRotate = tf.transformations.quaternion_about_axis(math.pi,(0,1,0))
        #create quaternion object for the rotational part
        q_camera = pyquaternion.Quaternion(r_camera[3],r_camera[0],r_camera[1],r_camera[2])

        #make the z a little deeper into the object
        z += .02
        #The map function will apply a operator element wise to two lists/tuples
        #Rotate then translate the x,y,z so that the pick point is with respect to the base
        t_pick = map(operator.add, q_camera.rotate([x,y,z]), t_camera)


        #make the pick rotation the same as the camera rotation TODO maybe align the pick vertically or something
        r_pick = tf.transformations.quaternion_multiply(angleRotate,r_camera)

        userdata.pick_pose = (t_pick,r_pick)


        #broadcast the pick so that we can view in rviz
        self.transform_broadcaster.sendTransform(t_pick,r_pick,rospy.Time.now(),"pick_point","base")


        img = bridge.imgmsg_to_cv2(image_msg, desired_encoding="passthrough")
        img.setflags(write=1)
        #cv2.drawContours(img,cnt,-1,(0,0,0))
        #cv2.imshow("test",img)
        #cv2.waitKey(100)

        #convert segmented structured numpy array back into ros message
        segmented_point_cloud_msg = ros_numpy.msgify(sensor_msgs.msg.PointCloud2, point_cloud_numpy)

        #add the correct frame id to the message so the point cloud is displayed in the correct spot in rviz
        segmented_point_cloud_msg.header.frame_id = "camera_rgb_optical_frame"

        #publish segmented point cloud message
        self.point_cloud_pub.publish(segmented_point_cloud_msg)

        userdata.item_to_pick = item_to_pick_name

        util.timers.stop("total_state_process_images")
        return 'success'
