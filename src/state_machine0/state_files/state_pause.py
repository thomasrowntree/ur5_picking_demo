import smach
from state_machine_utilities import State_Machine_Utilities

class StatePause(smach.State):
	"""
	StatePause handles all keyboard interrupts. It takes the previous state as input, gives the user a choice
	to either resume or shutdown, then returns to the previous state or goes to shutdown state.
	"""

	def __init__(self):
		smach.State.__init__(self, outcomes = ['resume_initialise','resume_move_to_capture',
												'resume_process_image', 'resume_move_above_object', 'resume_soft_pick',
												'resume_hard_pick', 'resume_drop_item', 'resume_hand', 'quit'],
							input_keys = ['prev_state'],
							output_keys = [])

	def execute(self, userdata):

		try:
			util = State_Machine_Utilities()
			print(util.timers)

			user_input = raw_input('\nThe state machine was paused. Resume (enter) or quit (ctrl+c)? ')
			print 'Resuming ' + userdata.prev_state
			outcome_key = 'resume_' + userdata.prev_state
			return outcome_key

		except KeyboardInterrupt:
			print '\nQuitting state machine'
			return 'quit'
