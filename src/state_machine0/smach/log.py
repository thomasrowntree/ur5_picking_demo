
import smach

__all__ = ['set_loggers','loginfo','logwarn','logerr','logdebug']

def loginfo(msg):
    print("[  INFO ] : "+str(msg))

def logwarn(msg):
    print("[  WARN ] : "+str(msg))

def logdebug(msg):
    print("[ DEBUG ] : "+str(msg))

def logerr(msg):
    print("[ ERROR ] : "+str(msg))

def set_loggers(info,warn,debug,error):
    """Override the SMACH logging functions."""
    smach.loginfo = info
    smach.logwarn = warn
    smach.logdebug = debug
    smach.logerr = error

def suppress_loggers():
	def log_pass(msg):
		pass
	smach.log.set_loggers(log_pass, log_pass, log_pass, log_pass)

def allow_loggers():
	smach.log.set_loggers(smach.log.loginfo, smach.log.logwarn, smach.log.logdebug, smach.log.logerr)

