import smach

from state_files import state_base, state_initialise, state_move_to_capture_pos, state_move_above_object, state_soft_pick
from state_files import state_hard_pick, state_drop_item, state_process_image, state_hand, state_pause, state_shutdown
def main():

	# create empty state machine (container) with one possible outcome:
	sm = smach.StateMachine(outcomes=['sm_quit'],
							input_keys=[],
							output_keys=[])


	# comment this out to receive smach log messages:
	smach.log.suppress_loggers()

	# open the container:
	with sm:
		# add each state, describe state transitions, and remap userdata variables to be passed between:

		# at the moment, the state machine begins at initialise, then cycles through the states until ctrl+c,
		# then goes to pause state then quits or goes back to previous state

		smach.StateMachine.add('INITIALISE', state_initialise.StateInitialise(),
								transitions = {'success': 'MOVE_TO_CAPTURE', 'paused': 'PAUSE'},
								remapping = {'current_state': 'sm_state', 'item_list': 'sm_item_list'})
		smach.StateMachine.add('MOVE_TO_CAPTURE', state_move_to_capture_pos.StateMoveToCapturePos(),
								transitions = {'success': 'PROCESS_IMAGE', 'move_failed':'MOVE_TO_CAPTURE', 'paused': 'PAUSE'},
								remapping = {'current_state': 'sm_state'})
		smach.StateMachine.add('PROCESS_IMAGE', state_process_image.StateProcessImage(),
								transitions = {'success': 'MOVE_ABOVE_OBJECT', 'paused': 'PAUSE', 'cant_see_item': 'PROCESS_IMAGE', 'hand_detected': 'HAND'},
								remapping = {'item_list': 'sm_item_list', 'item_to_pick': 'sm_item_name', 'current_state': 'sm_state', 'pick_pose' : 'sm_pick_pose','grip_width':'sm_grip_width'})
		smach.StateMachine.add('MOVE_ABOVE_OBJECT', state_move_above_object.StateMoveAboveObject(),
								transitions = {'soft_success': 'SOFT_PICK', 'hard_success': 'HARD_PICK', 'move_failed':'MOVE_TO_CAPTURE', 'paused': 'PAUSE'},
								remapping = {'item_to_pick_name': 'sm_item_name', 'current_state': 'sm_state', 'pick_pose' : 'sm_pick_pose'})
		smach.StateMachine.add('SOFT_PICK', state_soft_pick.StateSoftPick(),
								transitions = {'success': 'DROP_ITEM', 'move_failed':'MOVE_TO_CAPTURE','grip_failed':'MOVE_TO_CAPTURE', 'paused': 'PAUSE'},
								remapping = {'item_to_pick_name': 'sm_item_name', 'current_state': 'sm_state', 'pick_pose' : 'sm_pick_pose'})
		smach.StateMachine.add('HARD_PICK', state_hard_pick.StateHardPick(),
								transitions = {'success': 'DROP_ITEM', 'move_failed':'MOVE_TO_CAPTURE','grip_failed':'MOVE_TO_CAPTURE', 'paused': 'PAUSE'},
								remapping = {'item_to_pick_name': 'sm_item_name', 'current_state': 'sm_state', 'pick_pose' : 'sm_pick_pose', 'grip_width':'sm_grip_width'})
		smach.StateMachine.add('DROP_ITEM', state_drop_item.StateDropItem(),
								transitions = {'success': 'MOVE_TO_CAPTURE', 'move_failed':'MOVE_TO_CAPTURE','grip_failed':'MOVE_TO_CAPTURE', 'paused': 'PAUSE'},
								remapping = {'item_to_pick_name': 'sm_item_name', 'current_state': 'sm_state'})
		smach.StateMachine.add('HAND', state_hand.StateHand(),
								transitions = {'success': 'MOVE_TO_CAPTURE', 'move_failed': 'MOVE_TO_CAPTURE', 'paused': 'PAUSE'},
								remapping = {'current_state': 'sm_state'})
		smach.StateMachine.add('PAUSE', state_pause.StatePause(),
								transitions = {'resume_initialise': 'INITIALISE','resume_move_to_capture': 'MOVE_TO_CAPTURE',
											'resume_process_image': 'PROCESS_IMAGE', 'resume_move_above_object' : 'MOVE_ABOVE_OBJECT',
											'resume_soft_pick': 'SOFT_PICK', 'resume_hard_pick': 'HARD_PICK', 'resume_drop_item': 'DROP_ITEM',
											'resume_hand': 'HAND', 'quit': 'SHUTDOWN'},
								remapping = {'prev_state': 'sm_state'})
		smach.StateMachine.add('SHUTDOWN', state_shutdown.StateShutdown(),
								transitions = {'done': 'sm_quit'},
								remapping = {})
	outcome = sm.execute()
	

if __name__ == '__main__':
	main()
