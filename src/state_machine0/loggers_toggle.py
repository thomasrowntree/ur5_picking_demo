def suppress_loggers():
	def log_pass(msg):
		pass
	smach.log.set_loggers(log_pass, log_pass, log_pass, log_pass)

def allow_loggers():
	smach.log.set_loggers(smach.log.loginfo, smach.log.logwarn, smach.log.logdebug, smach.log.logerr)
