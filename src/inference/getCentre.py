#!/usr/bin/env python
#this is a ROS service node which takes an image and returns that image segmented
#The classes the image is segmented into are: background,baseball,monkey,pig,poo,poonicorn,fries and dice
import argparse
import os
import sys

import numpy as np

import rospy
import cv2
import roslib
from sensor_msgs.msg import Image
from std_msgs.msg import String
from cv_bridge import CvBridge, CvBridgeError

def getCentre(image,item):
    # Read image
    im = image
    #im = cv2.cvtColor(im,cv2.COLOR_GRAY2RGB)
    # Setup SimpleBlobDetector parameters.
    thresh = cv2.compare(im,item,0)
    im2, contours, hierarchy = cv2.findContours(thresh, 1, 2)
    biggest = 0
    for i in range(len(contours)):
        if (len(contours[i]) > len(contours[biggest])):
            biggest = i
    if (len(contours) != 0):
        if (len(contours[biggest]) > 25):
            cnt = contours[biggest]
            M = cv2.moments(cnt)
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
            print cx,cy
            return cx,cy
    return -1,-1
