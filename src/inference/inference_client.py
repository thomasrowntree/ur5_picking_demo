#!/usr/bin/env python
#this is a client node which repeatedly takes latest image from the camera, sends it to the segmenting service node and outputs segmented image to the screen
#some of these imports are probably unnecessary, but I don't want to risk taking one out and breaking it
import argparse
import os
import sys
import numpy as np

import torch                           ##uncomment this for when on robot computer
import torch.nn as nn                   ##uncomment this for when on robot computer
from PIL import Image as PImage         ##uncomment this for when on robot computer
from torch.autograd import Variable    ##uncomment this for when on robot computer
#these are for ROS and openCV stuff
from sensor_msgs.msg import Image       ##uncomment this for when on robot computer
from ur5_picking_demo.srv import *   ##uncomment this for when on robot computer
import rospy
import cv2
from cv_bridge import CvBridge, CvBridgeError
from getCentre import getCentre
import parameters


bridge = CvBridge()
cv2.waitKey(5)

from model import resnet101_bilinear as resnet101 # this is faster     ##uncomment this for when on robot computer
# from model import resnet101 as resnet101 # slow: bicubic resize

if __name__ == '__main__':
    #first node is initialised and service is declared as a local variable
    rospy.init_node('image_grabber')                       ###uncomment this
    inference = rospy.ServiceProxy('inference',Inference)  ###uncomment this
    while(True):
        #the next image published by the camera is then grabbed
        msg = rospy.wait_for_message(parameters.rgbTopic, Image)                     ###uncomment this
        #image = cv2.imread("/home/ryan/Pictures/ur5_picking_demo/src/inference/test.png", cv2.IMREAD_GRAYSCALE)
        #print image.shape
        #passed into the service to be segmented
        result = inference(msg)                    ###uncomment this
        #and converted from ROS to openCV to 3 channels so can be added to overlay
        cvResult = bridge.imgmsg_to_cv2(result.mask, desired_encoding="passthrough")  ###uncomment this\
        #cvResult = image                                                                ###comment this on robo computer
        threeChannelResult = cv2.convertScaleAbs(parameters.label_colours[cvResult])                ###uncomment this\
        #convert rgb image to opencv format and apply segmented image on top with 50% opacity
        original = bridge.imgmsg_to_cv2(msg, desired_encoding="passthrough")        ###uncomment this
        #original = image               ##comment this when on robot computer
        original = original[:,:,::-1]   ###uncomment this
        original = cv2.addWeighted(original,0.5,threeChannelResult,0.5,0)  ###uncomment this
        #add edges to the resulting image according to edges in the segmented image
        edges = cv2.Canny(cvResult,0,0)
        threeEdges = cv2.cvtColor(edges,cv2.COLOR_GRAY2RGB)
        #original = cv2.cvtColor(original,cv2.COLOR_GRAY2RGB)  ##comment this when on robot computer
        #original = cv2.add(original,threeEdges)
	for i in range(2,len(parameters.classes)):
            cx,cy = getCentre(cvResult,i)
            if (cx != -1):
                original[cy,cx][2] = 255
                original[cy,cx][1] = 0
                original[cy,cx][0] = 0

                original[cy+1,cx][2] = 255
                original[cy+1,cx][1] = 0
                original[cy+1,cx][0] = 0

                original[cy,cx+1][2] = 255
                original[cy,cx+1][1] = 0
                original[cy,cx+1][0] = 0

                original[cy-1,cx][2] = 255
                original[cy-1,cx][1] = 0
                original[cy-1,cx][0] = 0

                original[cy,cx-1][2] = 255
                original[cy,cx-1][1] = 0
                original[cy,cx-1][0] = 0
                cv2.putText(original, parameters.classes[i],(cx-(8*len(parameters.classes[i])),cy),cv2.FONT_HERSHEY_SIMPLEX,0.8,(0,0,255),2,cv2.LINE_AA)

        #show final resulting image to screen for 50 milliseconds
        cv2.imshow("overlay",(original))
        cv2.waitKey(1)
        #decrease the number to increase speed,
        #not sure what will happen if speed of client is faster than speed of service though
    #this is then repeated forever until ctrl-C closes the program
