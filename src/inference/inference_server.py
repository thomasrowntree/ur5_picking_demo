#!/usr/bin/env python
#this is a ROS service node which takes an image and returns that image segmented
#The classes the image is segmented into are: background,baseball,monkey,pig,poo,poonicorn,fries and dice
import argparse
import os
import sys

import numpy as np
import torch
import torch.nn as nn
from PIL import Image as PImage
from torch.autograd import Variable

from ur5_picking_demo.srv import *
import rospy
import cv2
import roslib
from sensor_msgs.msg import Image
from std_msgs.msg import String
from cv_bridge import CvBridge, CvBridgeError
import parameters
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + '/refinenet_pytorch/src')
from model import resnet101_bilinear as resnet101 # this is faster
# from model import resnet101 as resnet101 # slow: bicubic resize
class InferenceServer:
    #this is the service itself, the code that runs every time the service is called
    def handle_inference(self,req):
        # Read image
        img_ = self.bridge.imgmsg_to_cv2(req.photo, desired_encoding="passthrough")
        img = ((np.array(img_)[:, :, :] * self.IMG_SCALE - self.IMG_MEAN) / self.IMG_STD).transpose((2, 0, 1))[None]
        img_inp = Variable(torch.from_numpy(img).float().cuda(),
                           volatile=True)
         #Forward
        out = self.net.forward(img_inp)[:, :21] # IGNORE 255
        out_ = nn.Upsample(size=img.shape[2:], mode='bilinear')(out)
        out_np = out_[0].data.cpu().numpy().argmax(axis=0)
        #out = self.label_colours[out_np] ###uncomment this line to have the output be coloured instead of black and white
        out = out_np
        mask_ = self.bridge.cv2_to_imgmsg(out.astype(np.uint8), encoding="passthrough")
        #save_path = '{}/{}.png'.format("FOLDER_LOCATION_HERE", 'mask_') ##uncomment this line and the one below if you want the segmented image saved to a folder somewhere
        #mask_.save(save_path)
        #print("=> saved result at {}".format(save_path))
        return mask_

    #this is the code that only runs when the service node is first initialised in a terminal
    def inference_server(self):
        self.bridge = CvBridge()
        #this is only needed for colour output instead of black and white
        self.label_colours = np.array([(0,0,0)
                        # 0=background
                        ,(128,0,0),(0,128,0),(128,128,0),(0,0,128),(128,0,128)
                        # 1=aeroplane, 2=bicycle, 3=bird, 4=boat, 5=bottle
                        ,(0,128,128),(128,128,128),(64,0,0),(192,0,0),(64,128,0)
                        # 6=bus, 7=car, 8=cat, 9=chair, 10=cow
                        ,(192,128,0),(64,0,128),(192,0,128),(64,128,128),(192,128,128)
                        # 11=diningtable, 12=dog, 13=horse, 14=motorbike, 15=person
                        ,(0,64,0),(128,64,0),(0,192,0),(128,192,0),(0,64,128)])
        #these are Resnet values, changing them is not reccomended unless weights are retrained from scratch
        self.IMG_SCALE  = 1./255
        self.IMG_MEAN = np.array([0.485, 0.456, 0.406]).reshape((1, 1, 3))
        self.IMG_STD = np.array([0.229, 0.224, 0.225]).reshape((1, 1, 3))
        #now it loads the weights, this will take a few seconds
        print("now loading weights at ",parameters.weightsPath)
        self.net = resnet101()
        if os.path.isfile(parameters.weightsPath):
            checkpoint = torch.load(parameters.weightsPath)
            self.net.load_state_dict(checkpoint['state_dict'])
        else:
            raise ValueError("=> no checkpoint found at '{}'".format(parameters.weightsPath))
        _ = self.net.eval()
        _ = self.net.cuda()
        rospy.init_node('inference_server')
        s = rospy.Service('inference', Inference, self.handle_inference)
        #don't call the service until "and now I spin" is printed in the terminal, otherwise it's not ready to be called yet (hasn't finished setting itself up)
        print ("finished setting up, ready for service requests")
        rospy.spin()

if __name__ == '__main__':
    server = InferenceServer()
    server.inference_server()
