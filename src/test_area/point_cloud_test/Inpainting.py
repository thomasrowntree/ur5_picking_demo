#!/usr/bin/env python

import cv2
import numpy as np
from matplotlib import pyplot as plt
import thinning
import random
import ros_numpy
from skimage.restoration import inpaint


point_cloud = np.load("point_clound_000.np.npy")
point_cloud = ros_numpy.point_cloud2.split_rgb_field(point_cloud)
h,w = point_cloud.shape
rgb_img = np.empty([h,w,3],dtype=np.uint8)
rgb_img[:,:,0] = point_cloud["r"]
rgb_img[:,:,1] = point_cloud["g"]
rgb_img[:,:,2] = point_cloud["b"]

plt.figure(1)
plt.imshow(rgb_img)

x = point_cloud["x"]
y = point_cloud["y"]
z = point_cloud["z"]

# x = x[100:250,200:350]



inpaint_mask = np.isnan(x)


x[inpaint_mask] = 0
x = np.clip(x,-0.5,0.5) + 0.5

x = (np.round(x * 255)).astype(np.uint8)

plt.figure(2)
plt.imshow(x)

print(x.shape)
print(x.dtype)

inpainted = cv2.inpaint(x,inpaint_mask.astype(np.uint8),3,cv2.INPAINT_NS)

plt.figure(3)
plt.imshow(inpainted)
plt.show()

#rgb = point_cloud["rgb"]



# img = cv2.imread('erode_me4.png',0)
# ret,starting_img = cv2.threshold(img,127,1,cv2.THRESH_BINARY)
#
#
# img = starting_img.copy()INPAINT_NS
# border = 1
# img[:,:border] = 0
# img[:,-border:] = 0
# img[:border,:] = 0
# img[-border:,:] = 0
#
# thin = thinning.guo_hall_thinning(img)
# border = 10
# thin[:,:border] = 0
# thin[:,-border:] = 0
# thin[:border,:] = 0
# thin[-border:,:] = 0
# picks = np.argwhere(thin)
# print(picks)
# while True:
#     n,_ = picks.shape
#     i = random.randint(0,n)
#
#
#     plt.subplot(1,2,1)
#     plt.imshow(starting_img+thin)
#     plt.subplot(1,2,2)
#     plt.imshow(thin)
#     plt.show()


#get masks of chosen index
#remove regions touching the edge
#thin remaining regions
#pick random points from thinned line
#find the line of best at each points
#look along perpendiculars for grip points
