#!/usr/bin/env python

import cv2
import numpy as np
from matplotlib import pyplot as plt
import thinning #https://pypi.python.org/pypi/thinning
import random
import os
import math


def find_hollow_object_pick_point(segmentation_mask,image_for_annotation=None):
    """ Takes a binary segmenation mask where all pixels are 0 besides the mask
        of the object you want to pick.

        The algorithm will thin the mask down to a single line then pick
        a random point on the line and aligning the gripper with the angle of
        the line
    """

    #CONSTANTS
    angle_window_size = 5 #pixels. the number of pixels either side of the centre pixel used for getting line of best fit

    #use the thinning library to thin the mask down to single line
    thinned_mask = thinning.guo_hall_thinning(segmentation_mask)

    #remove any thinned lines that are too close to the edge
    border = angle_window_size+1
    thinned_mask[:,:border] = 0
    thinned_mask[:,-border:] = 0
    thinned_mask[:border,:] = 0
    thinned_mask[-border:,:] = 0

    #create a new 2D array of all the pixel coordinates
    pixels_coordinates = np.argwhere(thinned_mask)

    #get the number of pixels
    num_of_pixels,_ = pixels_coordinates.shape

    #TODO tom: return something meaningfull
    if num_of_pixels < 20:
        return False

    #for test in range(5):
    
    #choose a random pixel pick point
    random_index = random.randint(0,num_of_pixels-1)

    #get the coordinates of the random pixel
    cy,cx = pixels_coordinates[random_index]

    #use the angle_window_size to crop out a small region around the point
    cy1 = cy-angle_window_size
    cy2 = cy + angle_window_size + 1
    cx1 = cx-angle_window_size
    cx2 = cx + angle_window_size + 1
    line_region = thinned_mask[cy1:cy2, cx1:cx2]

    #get the coordinates of the pixel in the line
    line_points = np.argwhere(line_region)

    #fit a line to the pixels and get the x and y slopes
    line_params = cv2.fitLine(line_points, 2, 0, 0.01, 0.01)
    #get the slope or velocity of the line
    vcy = line_params[0,0]
    vcx = line_params[1,0]

    #if the line is pointing down, flip it so its pointing up
    if vcy > 0:
        vcy = - vcy
        vcx = - vcx

    #calculate the angle
    angle = -math.atan(vcx/vcy)

    #Draw a line on the image
    if image_for_annotation is not None:
        line_length = 40
        point1 = (int(cx), int(cy))
        point2 = (int(cx + vcx * line_length), int(cy + vcy * line_length))
        cv2.line(image_for_annotation,point1,point2,(255,0,0),2)

        #Draw a point on the pixel we want to pick from
        cv2.circle(image_for_annotation,(cx,cy),3,(0,0,0),-1)

        font = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = 0.5
        font_thickness = 1
        text = "%2.0f"%(angle/math.pi*180)
        (w,h),_ = cv2.getTextSize(text,font,font_scale,font_thickness)
        cv2.putText(image_for_annotation,text,(cx-w/2,cy+h+4), font, font_scale,(0,255,0),font_thickness,cv2.LINE_AA)


    return cx,cy,angle


for img_number in range(0,177):
    item_id = 30

    img_dir = "/home/thomas/ros_ws/src/ur5_picking_demo/src/capturing/dataset_20180208"

    img_path = os.path.join(img_dir,"urIMG%04i.png"%img_number)
    map_path = os.path.join(img_dir,"urIMG%04i_a.png"%img_number)

    bgr_img = cv2.imread(img_path)
    rgb_img = cv2.cvtColor(bgr_img,cv2.COLOR_BGR2RGB)
    map_img = cv2.imread(map_path,0)



    for item_id in range(15,181,15):
        item_mask = np.logical_and(map_img > (item_id-10),map_img < (item_id+10)).astype(np.uint8)


        find_hollow_object_pick_point(item_mask,rgb_img)
        # thin = thinning.guo_hall_thinning(item_mask)
        #
        # border = 10
        # thin[:,:border] = 0
        # thin[:,-border:] = 0
        # thin[:border,:] = 0
        # thin[-border:,:] = 0
        #
        # picks = np.argwhere(thin)
        #
        # if len(picks) > 50:
        #     for _ in range(50):
        #         n,_ = picks.shape
        #         i = random.randint(0,n-1)
        #         pick_row,pick_col = picks[i,0],picks[i,1]
        #
        #
        #         window_size = 5
        #         line_region = thin[pick_row-window_size : pick_row + window_size + 1, pick_col-window_size : pick_col + window_size + 1]
        #         line_points = np.argwhere(line_region)
        #         line_params = cv2.fitLine(line_points, 2, 0, 0.01, 0.01)
        #         vel_row = line_params[0,0]
        #         vel_col = line_params[1,0]
        #
        #         vel_row_perpendicular = vel_col
        #         vel_col_perpendicular = -vel_row
        #
        #         line_length = 10
        #         point1 = (int(pick_col + vel_col_perpendicular * line_length), int(pick_row + vel_row_perpendicular * line_length))
        #         point2 = (int(pick_col - vel_col_perpendicular * line_length), int(pick_row - vel_row_perpendicular * line_length))
        #         cv2.line(rgb_img,point1,point2,(255,0,0))
        #         cv2.circle(rgb_img,(pick_col,pick_row),2,(0,0,0),-1)


    # plt.figure(2)
    # plt.imshow(line_region)

    plt.figure(1)
    # plt.subplot(1,2,1)
    plt.imshow(rgb_img)
    # plt.subplot(1,2,2)
    # plt.imshow(item_mask)
    plt.draw()
    plt.pause(0.1)
    raw_input("press the keys yo")


#get masks of chosen index
#remove regions touching the edge
#thin remaining regions
#pick random points from thinned line
#find the line of best at each points
#look along perpendiculars for grip points
