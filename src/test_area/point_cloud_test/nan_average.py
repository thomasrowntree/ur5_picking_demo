#!/usr/bin/env python

import cv2
import numpy as np
from matplotlib import pyplot as plt
import thinning
import random
import ros_numpy
from skimage.restoration import inpaint


point_cloud = np.load("point_clound_000.np.npy")
point_cloud = ros_numpy.point_cloud2.split_rgb_field(point_cloud)
h,w = point_cloud.shape
rgb_img = np.empty([h,w,3],dtype=np.uint8)
rgb_img[:,:,0] = point_cloud["r"]
rgb_img[:,:,1] = point_cloud["g"]
rgb_img[:,:,2] = point_cloud["b"]

plt.figure(1)
plt.imshow(rgb_img)

x = point_cloud["x"]
y = point_cloud["y"]
z = point_cloud["z"]


average = np.nanmean(z)
nans = np.isnan(z)
z_no_nans = z.copy()
z_no_nans[nans] = average

z_blur =cv2.GaussianBlur(z_no_nans,(9,9),20)
print(average)

plt.subplot(2,2,1)
plt.imshow(z)
plt.subplot(2,2,2)
plt.imshow(nans)
plt.subplot(2,2,3)
plt.imshow(z_no_nans)
plt.subplot(2,2,4)
plt.imshow(z_blur)
plt.show()
