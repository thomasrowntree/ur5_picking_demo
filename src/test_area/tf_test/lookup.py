#!/usr/bin/env python

import rospy
import tf
import time
import math
import tf2_ros

rospy.init_node("ls_node")

br = tf.TransformBroadcaster()
ls = tf.TransformListener()

def tryLookupTransform(from_frame,to_frame,frame_time):
    start_time = time.time()
    while True:
        try:
            (trans,rot) = ls.lookupTransform(from_frame, to_frame, frame_time)
            return (trans,rot)
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
            if time.time() - start_time > 2.0:
                raise
        rospy.sleep(0.01)

while not rospy.is_shutdown():

    try:
        #get constant time


        (trans,rot) = tryLookupTransform('gripper', 'ur_tcp', rospy.Time(0))

        br.sendTransform(trans,rot,rospy.Time.now(),'ur_tcp_target',"pick")
        #ls.waitForTransform('base', 'temp', rospy.Time(), rospy.Duration(4.0))
        (trans,rot) = tryLookupTransform('base', 'ur_tcp_target', rospy.Time(0))
        #br.sendTransform(trans,rot,rospy.Time.now(),"target","base")
        print(trans)
    except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException, tf2_ros.TransformException) as e:
        print(e)

    time.sleep(0.1)
