#!/usr/bin/env python

import rospy
import tf
import tf2_ros
import time
import math
import geometry_msgs.msg

rospy.init_node("static_tf_node")

br = tf.TransformBroadcaster()
ls = tf.TransformListener()
sbr = tf2_ros.StaticTransformBroadcaster()
#time.sleep(0.5)


def tryLookupTransform(from_frame,to_frame,frame_time):
    start_time = time.time()
    while True:
        try:
            (trans,rot) = ls.lookupTransform(from_frame, to_frame, frame_time)
            return (trans,rot)
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
            if time.time() - start_time > 2.0:
                raise
        rospy.sleep(0.01)

while not rospy.is_shutdown():
    raw_input("Press enter to capture")

    static_transformStamped = geometry_msgs.msg.TransformStamped()
    static_transformStamped.header.stamp = rospy.Time.now()
    static_transformStamped.header.frame_id = "camera"
    static_transformStamped.child_frame_id = "pick"
    static_transformStamped.transform.translation.x = 0.5
    static_transformStamped.transform.rotation.w = 1.0

    sbr.sendTransform(static_transformStamped)
    print("static")







    # #get constant time
    # now = rospy.Time.now()
    #
    # #publish temp pick with respect to camera frame
    # br.sendTransform((0.5,0,0),(0,0,0,1),now,"temp_pick","camera")
    #
    # #get transform from base to temp pick at the same time
    # (trans,rot) = tryLookupTransform('base', 'temp_pick', now)
    #
    # for i in range(100):
    #     #publish this again as the pick point with respect to base frame with a future time
    #     br.sendTransform(trans,rot,rospy.Time.now() ,'pick',"base")
    #     rospy.sleep(0.01)





    # try:
    #     (trans,rot) = tryLookupTransform('base', 'temp_pick', now)
    #     br.sendTransform(trans,rot,now + rospy.Duration(10),'pick',"base")
    #
    #     (trans,rot) = tryLookupTransform('gripper', 'ur_tcp', rospy.Time(0))
    #     br.sendTransform(trans,rot,now + rospy.Duration(10),'ur_tcp_target',"pick")
    #
    #
    #
    # except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException, tf2_ros.TransformException) as e:
    #     print(e)
