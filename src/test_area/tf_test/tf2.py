#!/usr/bin/env python

import rospy
import tf
import tf2_ros
import time
import math

rospy.init_node("tf2_node")

br = tf.TransformBroadcaster()
ls = tf.TransformListener()
#time.sleep(0.5)


def tryLookupTransform(from_frame,to_frame,frame_time):
    start_time = time.time()
    while True:
        try:
            (trans,rot) = ls.lookupTransform(from_frame, to_frame, frame_time)
            return (trans,rot)
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
            print("lookup failed")
            if time.time() - start_time > 2.0:
                raise
        rospy.sleep(0.01)

while not rospy.is_shutdown():
    raw_input("Press enter to capture")

    #get constant time
    now = rospy.Time.now()

    #publish temp pick with respect to camera frame
    br.sendTransform((0.5,0,0),(0,0,0,1),now,"pick","camera")

    # #get transform from base to temp pick at the same time
    # (trans,rot) = tryLookupTransform('base', 'temp_pick', now)
    #
    # br.sendTransform(trans,rot,rospy.Time.now() ,'pick',"base")






    # try:
    #     (trans,rot) = tryLookupTransform('base', 'temp_pick', now)
    #     br.sendTransform(trans,rot,now + rospy.Duration(10),'pick',"base")
    #
    #     (trans,rot) = tryLookupTransform('gripper', 'ur_tcp', rospy.Time(0))
    #     br.sendTransform(trans,rot,now + rospy.Duration(10),'ur_tcp_target',"pick")
    #
    #
    #
    # except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException, tf2_ros.TransformException) as e:
    #     print(e)
