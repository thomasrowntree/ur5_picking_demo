#!/usr/bin/env python

import rospy
import tf
import time
import math

rospy.init_node("tf1_node")

br = tf.TransformBroadcaster()
rate = rospy.Rate(30)
angle = 0.0
while not rospy.is_shutdown():
    angle += 1
    x = math.cos(angle/180.0*math.pi)
    y = math.sin(angle/180.0*math.pi)

    br.sendTransform((x,y,0),(0,0,0,1),rospy.Time.now(),"ur_tcp","base")
    br.sendTransform((0,0,0.3),(0,0,0,1),rospy.Time.now(),"camera","ur_tcp")
    br.sendTransform((0.3,0,0),(0,0,0,1),rospy.Time.now(),"gripper","ur_tcp")
    # br.sendTransform((1,0,0),
    # tf.transformations.quaternion_about_axis(math.radians(angle),(0,0,1)),
    # rospy.Time.now(),"ur_tcp","base")

    rate.sleep()
