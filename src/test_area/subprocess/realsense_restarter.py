#!/usr/bin/env python

"""
This script is a workaround that restarts the realsense camera node
if now points clouds have been received for a while.
"""

import subprocess
import time
import rospy
import sensor_msgs.msg



args = ["roslaunch", "realsense_camera", "sr300_nodelet_rgbd.launch"]

rospy.init_node("realsense_camera_restarter", anonymous  = True)


while not rospy.is_shutdown():
    try:
        #roslaunch realsense node in a new process
        sub = subprocess.Popen(args)
        rospy.loginfo("Realsense Restarter has called: %s" % " ".join(args))

        #wait for the first point cloud with a long timeout as it can take a while to initialise
        point_cloud_msg = rospy.wait_for_message('/camera/depth_registered/points', sensor_msgs.msg.PointCloud2, timeout=20)
        rospy.loginfo("The Realsense is working")

        #Keep trying to read points clouds with a short timeout.
        #If a timeout exception occures the whole thing will restart
        while not rospy.is_shutdown():
            point_cloud_msg = rospy.wait_for_message('/camera/depth_registered/points', sensor_msgs.msg.PointCloud2, timeout=0.5)

    except rospy.exceptions.ROSException as re:
        if "timeout" in re.message:
            rospy.logerr("Realsense timeout occured. Realsense Restarter will restart it")

    finally:
        #be sure to terminate the realsense process before quiting or restarting
        sub.terminate()
        rospy.loginfo("Terminating Realsense Process")

        #poll until it has terminated
        while sub.poll() is None:
            time.sleep(0.5)
            rospy.logwarn("Waiting for the Realsense process to terminate")
            
        rospy.logwarn("Realsense process has terminated")
