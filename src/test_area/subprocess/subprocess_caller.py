#!/usr/bin/env python

import subprocess
import time


sub = subprocess.Popen(["roslaunch", "realsense_camera", "sr300_nodelet_rgbd.launch"])
print(sub.pid)
print(sub.poll())

raw_input("Press any key to end")

sub.terminate()

while sub.poll() is None:
    time.sleep(0.1)
    print(sub.poll())
    print(sub.pid)
