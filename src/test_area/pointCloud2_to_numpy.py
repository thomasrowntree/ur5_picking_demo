#!/usr/bin/env python

#https://github.com/eric-wieser/ros_numpy

import rospy
import ros_numpy
import numpy as np
from sensor_msgs.msg import PointCloud2, PointField

def callback(data):
    np_array = ros_numpy.numpify(data)
    print(np_array.shape)
    print(np_array.dtype)
    print(np_array.dtype.names)
    crop = np_array[239:242,319:322]
    print(np.nanmean(crop["z"]))
    print("yep")

rospy.init_node("pointcloud_test")
rospy.Subscriber("/camera/depth_registered/points",PointCloud2,callback)
rospy.spin()
