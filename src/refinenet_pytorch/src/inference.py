"""Run an inference on a single image."""

import argparse
import os
import sys

import numpy as np
import torch
import torch.nn as nn
from PIL import Image
from torch.autograd import Variable

from model import resnet101_bilinear as resnet101 # this is faster
# from model import resnet101 as resnet101 # slow: bicubic resize

label_colours = np.array([(0,0,0)
                # 0=background
                ,(128,0,0),(0,128,0),(128,128,0),(0,0,128),(128,0,128)
                # 1=aeroplane, 2=bicycle, 3=bird, 4=boat, 5=bottle
                ,(0,128,128),(128,128,128),(64,0,0),(192,0,0),(64,128,0)
                # 6=bus, 7=car, 8=cat, 9=chair, 10=cow
                ,(192,128,0),(64,0,128),(192,0,128),(64,128,128),(192,128,128)
                # 11=diningtable, 12=dog, 13=horse, 14=motorbike, 15=person
                ,(0,64,0),(128,64,0),(0,192,0),(128,192,0),(0,64,128)])

IMG_SCALE  = 1./255
IMG_MEAN = np.array([0.485, 0.456, 0.406]).reshape((1, 1, 3))
IMG_STD = np.array([0.229, 0.224, 0.225]).reshape((1, 1, 3))


def get_arguments():
    """Parse all the arguments provided from the CLI.

    Returns:
      A list of parsed arguments.
    """
    parser = argparse.ArgumentParser(description="Model Zoo Networks")
    parser.add_argument("--img-path", type=str, required=True,
                        help="Path to the input image ([png, jpg]).")
    parser.add_argument("--output-folder", type=str, default='./',
                        help="Where to save predicted mask.")
    parser.add_argument("--restore-from", type=str,
                        default='./../weights/rf.ckpt',
                        help="Path to the weights.")
    return parser.parse_args()

def main():
    """Main function."""
    args = get_arguments()
    net = resnet101()
    if os.path.isfile(args.restore_from):
        checkpoint = torch.load(args.restore_from)
        net.load_state_dict(checkpoint['state_dict'])
    else:
        raise ValueError("=> no checkpoint found at '{}'".format(args.restore_from))
    _ = net.eval()
    _ = net.cuda()
    if not os.path.exists(args.output_folder):
        os.makedirs(args.output_folder)
    # Read image
    img_ = Image.open(args.img_path)
    img = ((np.array(img_) * IMG_SCALE - IMG_MEAN) / IMG_STD).transpose((2, 0, 1))[None]
    img_inp = Variable(torch.from_numpy(img).float().cuda(),
                       volatile=True)
    # Forward
    out = net.forward(img_inp)[:, :21] # IGNORE 255
    out_ = nn.Upsample(size=img.shape[2:], mode='bilinear')(out)
    out_np = out_[0].data.cpu().numpy().argmax(axis=0)
    out = label_colours[out_np]
    mask = Image.fromarray(out.astype(np.uint8))
    save_path = '{}/{}.png'.format(args.output_folder, 'mask')
    mask.save(save_path)
    print("=> saved result at {}".format(save_path))

if __name__ == '__main__':
    main()
