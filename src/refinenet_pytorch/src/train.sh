# stage1 - (COCO + SBD + VOC)
python main.py --restore-from ./../weights/rf.ckpt --num-steps 200 --learning-rate 2.5e-5 --random-seed 42 --snapshot-dir ./checkpoints_dataset_20180208_2
# stage2 - (SBD + VOC)
#nohup python main.py --data-list ./../dataset/train+.lst --restore-from ./coco/ep20_checkpoint.pth.tar --num-steps 40 --learning-rate 2.5e-5 --random-seed 42 > $1&
# stage3 - (VOC)
#nohup python main.py --data-list ./../dataset/train.lst --restore-from ./sbd/ep40_checkpoint.pth.tar --num-steps 60 --learning-rate 5e-6 --random-seed 42 > $1&
