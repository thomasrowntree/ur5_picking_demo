"""Run an inference on a single image."""

import argparse
import os
import sys
import matplotlib.pyplot as plt

import numpy as np
import torch
import torch.nn as nn
from PIL import Image
from torch.autograd import Variable

from model import resnet101_bilinear as resnet101 # this is faster
# from model import resnet101 as resnet101 # slow: bicubic resize

label_colours = np.array([(0,0,0)
                # 0=background
                ,(200,0,200),(33,67,101),(0,0,255),(52,105,159),(0,255,0)
                # 1=others, 2=poo, 3=chips, 4=monkey, 5=poonicorn
                ,(203, 192, 255),(255,0,0),(255,255,255)])
                #6 = pig, 7 = ball, 8 = die

IMG_SCALE  = 1./255
IMG_MEAN = np.array([0.485, 0.456, 0.406]).reshape((1, 1, 3))
IMG_STD = np.array([0.229, 0.224, 0.225]).reshape((1, 1, 3))


def get_arguments():
    """Parse all the arguments provided from the CLI.

    Returns:
      A list of parsed arguments.
    """
    parser = argparse.ArgumentParser(description="Model Zoo Networks")
    parser.add_argument("--img-path", type=str, required=True,
                        help="Path to the input image ([png, jpg]).")
    parser.add_argument("--output-folder", type=str, default='./predictions/',
                        help="Where to save predicted mask.")
    parser.add_argument("--restore-from", type=str,
                        default='./../weights/rf.ckpt',
                        help="Path to the weights.")
    return parser.parse_args()

def main():
    """Main function."""
    args = get_arguments()
    net = resnet101()
    if os.path.isfile(args.restore_from):
        checkpoint = torch.load(args.restore_from)
        net.load_state_dict(checkpoint['state_dict'])
    else:
        raise ValueError("=> no checkpoint found at '{}'".format(args.restore_from))
    _ = net.eval()
    _ = net.cuda()
    if not os.path.exists(args.output_folder):
        os.makedirs(args.output_folder)

    net.dropout3.train()
    net.dropout4.train()
    net.dropout_clf.train()

    # Read image
    img_ = Image.open(args.img_path)
    img = ((np.array(img_) * IMG_SCALE - IMG_MEAN) / IMG_STD).transpose((2, 0, 1))[None]
    img_inp = Variable(torch.from_numpy(img).float().cuda(),
                       volatile=True)

    my_out = []
    for i in range(100):
        # Forward
        out = net.forward(img_inp)[:,:] # IGNORE 255
        out_ = nn.Upsample(size=img.shape[2:], mode='bilinear')(out)
        #out_np = out_[0].data.cpu().numpy().argmax(axis=0)
        out_np = out_[0].data.cpu().numpy()
        my_out.append(out_np)
        print(i)
        # out_np = out_[0].data.cpu().numpy()
        # print(out_np.shape)
        # out = np.abs(out_np[0,:,:])
        # out = label_colours[out_np]
        # out = out[:,:,::-1]
        # mask = Image.fromarray(out.astype(np.uint8))
        # save_path = '{}/mask{}.png'.format(args.output_folder, i)
        # mask.save(save_path)
        # print("=> saved result at {}".format(save_path))

    # Get the labels
    total_array = np.array(my_out)
    n,c,h,w = total_array.shape

    print(total_array.shape)
    alphas = total_array.mean(axis=0)
    print(alphas.shape)
    sigmas = total_array.std(axis=0)
    print(sigmas.shape)
    y_pred = alphas.argmax(axis=0)
    print(y_pred.shape)
    alpha_max = alphas.max(axis=0)
    print(alpha_max.shape)

    for i in range(c):
        plt.figure(i)
        plt.imshow(sigmas[i,:,:])
    plt.figure(c+1)
    plt.imshow(alpha_max)

    #color = label_colours[out_np]

    plt.show()

    return total_array

    #np.save(, '/tmp/mymat.npy')

if __name__ == '__main__':
    main()
