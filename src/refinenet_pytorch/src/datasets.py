"""Datasets implementations."""

from __future__ import print_function, division
import os
import torch
from skimage import io, transform
import numpy as np
from torch.utils.data import Dataset
from torchvision import transforms, utils
import cv2
import random

from PIL import Image
# Ignore warnings
import warnings
warnings.filterwarnings("ignore")


class Rescale(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or tuple): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        self.output_size = output_size

    def __call__(self, sample):
        image, mask = sample['image'], sample['mask']

        h, w = image.shape[:2]
        if isinstance(self.output_size, int):
            if h > w:
                new_h, new_w = self.output_size * h / w, self.output_size
            else:
                new_h, new_w = self.output_size, self.output_size * w / h
        else:
            new_h, new_w = self.output_size

        new_h, new_w = int(new_h), int(new_w)

        img = cv2.resize(image, (new_w, new_h), interpolation=cv2.INTER_CUBIC)
        msk = cv2.resize(mask, (new_w, new_h), interpolation=cv2.INTER_NEAREST)

        return {'image': img, 'mask' : msk}

class RandomScale(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or tuple): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self, scales, shorter_side):
        assert isinstance(scales, list)
        assert isinstance(shorter_side, int)
        self.scales = scales
        self.shorter_side = shorter_side

    def __call__(self, sample):
        image, mask = sample['image'], sample['mask']

        #l, h = self.scales
        #assert h >= l, "Upper bound must be higher than the lower bound"
        scale = random.choice(self.scales) #np.random.uniform(low=l, high=h)
        min_side = min(image.shape[:2])
        if min_side < self.shorter_side:
            scale *= (self.shorter_side * 1. / min_side)
        img = cv2.resize(image, None, fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
        msk = cv2.resize(mask, None, fx=scale, fy=scale, interpolation=cv2.INTER_NEAREST)
        return {'image': img, 'mask' : msk}

class RandomMirror(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or tuple): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
    """

    def __init__(self):
        pass
    def __call__(self, sample):
        image, mask = sample['image'], sample['mask']
        do_mirror = np.random.randint(2)
        if do_mirror:
            image = cv2.flip(image, 1)
            mask = cv2.flip(mask, 1)
        return {'image': image, 'mask' : mask}

class Normalize(object):
    """Normalize an tensor image with mean and standard deviation.
    Given mean: (R, G, B) and std: (R, G, B),
    will normalize each channel of the torch.*Tensor, i.e.
    channel = (channel - mean) / std
    Args:
        mean (sequence): Sequence of means for R, G, B channels respecitvely.
        std (sequence): Sequence of standard deviations for R, G, B channels
            respecitvely.
    """

    def __init__(self, scale, mean, std):
        self.scale = scale
        self.mean = mean
        self.std = std

    def __call__(self, sample):
        """
        Args:
            tensor (Tensor): Tensor image of size (C, H, W) to be normalized.
        Returns:
            Tensor: Normalized image.
        """
        image = sample['image']
        # TODO: make efficient
        return {'image': (self.scale * image - self.mean) / self.std, 'mask' : sample['mask']}

class RandomCrop(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size, scale):
        assert isinstance(output_size, (int, tuple))
        assert isinstance(scale, (float, int))
        self.crop_size = output_size
        self.scale = scale
	#if isinstance(output_size, int):
        #    self.output_size = (output_size, output_size)
        #else:
        #    assert len(output_size) == 2
        #    self.output_size = output_size

    def __call__(self, sample):
        image, mask = sample['image'], sample['mask']

        h, w = image.shape[:2]
        crop_size = min(self.crop_size, h, w)
        if crop_size % 2 != 0:
            crop_size -= 1
        new_h = new_w = crop_size
        top = np.random.randint(0, h - new_h + 1)
        left = np.random.randint(0, w - new_w + 1)

        image = image[top: top + new_h,
                        left: left + new_w]
        mask = mask[top: top + new_h,
                    left: left + new_w]
        if self.scale != 1.:
            new_h = int(new_h * self.scale)
            if new_h % 2 != 0:
                new_h -= 1
            image = cv2.resize(image, (new_h, new_h), interpolation=cv2.INTER_CUBIC)
            # No need to resize mask as it will be resized later for loss
            #mask = cv2.resize(mask, (new_h, new_h), interpolation=cv2.INTER_NEAREST)
        return {'image': image, 'mask': mask}


class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        image, mask = sample['image'], sample['mask']

        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        image = image.transpose((2, 0, 1))
#        if len(mask.shape) == 2:
#            mask = mask[:, :]
        # Will keep mask in numpy to resize later
        return {'image': torch.from_numpy(image),
                'mask': mask}


class PascalVOCDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, data_file, root_dir, transform=None):
        """
        Args:
            data_file (string): Path to the data file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        with open(data_file, 'rb') as f:
            datalist = f.readlines()
        try:
            self.datalist = [(k, v) for k, v in map(lambda x: x.strip('\n').split('\t'), datalist)]
        except ValueError: # Adhoc for test.
            self.datalist = [(k, k) for k in map(lambda x: x.strip('\n'), datalist)]
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.datalist)

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir, self.datalist[idx][0])
        msk_name = os.path.join(self.root_dir, self.datalist[idx][1])
        def read_image(x):
            img_arr = np.array(Image.open(x))
            if len(img_arr.shape) == 2: # grayscale
                img_arr = np.tile(img_arr, [3, 1, 1]).transpose(1, 2, 0)
            return img_arr
        image = read_image(img_name)
        mask = np.array(Image.open(msk_name))[:, :, 0]#io.imread(msk_name)
        mask = (mask / 15)
        if np.any(mask > 12):
            print(msk_name)
        if img_name != msk_name:
            assert len(mask.shape) == 2, 'Masks must be encoded without colourmap'
        sample = {'image': image, 'mask': mask}
        if self.transform:
            sample = self.transform(sample)
        return sample
