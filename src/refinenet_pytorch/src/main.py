"""Main script with training and validation.

During validation, forward pass is performed, and segmentation
masks are saved as numpy-files.
"""

import torch
import torch.nn as nn
import math
from torch.autograd import Variable
import torch.nn.functional as F
import torch.utils.model_zoo as model_zoo
from torchvision import transforms
from torch.utils.data import DataLoader
import torch.backends.cudnn as cudnn

from datasets import PascalVOCDataset, Rescale, RandomCrop, RandomMirror, RandomScale, ToTensor, Normalize
from model import resnet101_bilinear as resnet101 # this is faster
# from model import resnet101 as resnet101 # slow: bicubic resize
from utils import fast_cm, compute_iu

import argparse
import os
import shutil
import time

import cv2
import logging

import numpy as np

from copy import deepcopy
import parameters

logging.basicConfig(level=logging.INFO)

## PREPROCESSING
FINAL_SCALE = 1.2 # UPSAMPLE BY THIS AMOUNT BEFORE FORWARD (ONLY FOR TRAINING)
IMG_SCALE  = 1./255
IMG_MEAN = np.array([0.485, 0.456, 0.406]).reshape((1, 1, 3))
IMG_STD = np.array([0.229, 0.224, 0.225]).reshape((1, 1, 3))
SCALES = [0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3]

## DEFAULT VALUES
BATCH_SIZE = 1
DATA_DIRECTORY = '/home/ur/catkin_ws/src/ur5_picking_demo/src/capturing/dataset_20180208/'
DATA_LIST_PATH = '/home/ur/catkin_ws/src/ur5_picking_demo/src/capturing/dataset_20180208/train.txt'
DATA_VAL_LIST_PATH = '/home/ur/catkin_ws/src/ur5_picking_demo/src/capturing/dataset_20180208/test.txt'
CROP_SIZE = 400
LEARNING_RATE = 2.5e-5
MOMENTUM = 0.9
NUM_CLASSES = 13
NUM_STEPS = 400 # NUM_EPOCHS
POWER = 0.9
PRINT_EVERY = 1
RANDOM_SEED = 42
RESTORE_FROM = './../weights/rf.ckpt'
SHORTER_SIDE = 450
SNAPSHOT_DIR = './checkpoints_dataset_20180208_2'
WEIGHT_DECAY = 5e-4
VAL_EVERY = 5 # EVERY EPOCH

def get_arguments():
    """Parse all the arguments provided from the CLI.

    Returns:
      A list of parsed arguments.
    """
    parser = argparse.ArgumentParser(description="DeepLab-ResNet Network")
    parser.add_argument("--batch-size", type=int, default=BATCH_SIZE,
                        help="Number of images sent to the network in one step.")
    parser.add_argument("--data-dir", type=str, default=DATA_DIRECTORY,
                        help="Path to the directory containing the PASCAL VOC dataset.")
    parser.add_argument("--data-list", type=str, default=DATA_LIST_PATH,
                        help="Path to the file listing the images in the dataset.")
    parser.add_argument("--data-val-list", type=str, default=DATA_VAL_LIST_PATH,
                        help="Path to the file listing the images in the val dataset.")
    parser.add_argument("--evaluate", action="store_true",
                        help="Whether to perform evaluation.")
    parser.add_argument("--crop-size", type=int, default=CROP_SIZE,
                        help="Size of crop.")
    parser.add_argument("--learning-rate", type=float, default=LEARNING_RATE,
                        help="Base learning rate for training with polynomial decay.")
    parser.add_argument("--momentum", type=float, default=MOMENTUM,
                        help="Momentum component of the optimiser.")
    parser.add_argument("--not-restore-new", action="store_true",
                        help="Whether to not restore newly added layers.")
    parser.add_argument("--num-classes", type=int, default=NUM_CLASSES,
                        help="Number of classes to predict (including background).")
    parser.add_argument("--num-steps", type=int, default=NUM_STEPS,
                        help="Number of training steps.")
    parser.add_argument("--power", type=float, default=POWER,
                        help="Decay parameter to compute the learning rate.")
    parser.add_argument("--print-every", type=int, default=PRINT_EVERY,
            			help="Print information every often.")
    parser.add_argument("--random-seed", type=int, default=RANDOM_SEED,
                        help="Random seed to have reproducible results.")
    parser.add_argument("--restore-from", type=str, default=RESTORE_FROM,
                        help="Where restore model parameters from.")
    parser.add_argument("--shorter-side", type=int, default=SHORTER_SIDE,
                        help="Resize shorter side of image to this value before cropping.")
    parser.add_argument("--snapshot-dir", type=str, default=SNAPSHOT_DIR,
                        help="Where to save snapshots of the model.")
    parser.add_argument("--weight-decay", type=float, default=WEIGHT_DECAY,
                        help="Regularisation parameter for L2-loss.")
    parser.add_argument("--val-every", type=int, default=VAL_EVERY,
                        help="Perform validation every often.")
    return parser.parse_args()

def save_checkpoint(state, epoch, filename='checkpoint.pth.tar'):
    torch.save(state, '{}/ep{}_{}'.format(args.snapshot_dir, epoch, filename))

def train(train_loader, model, criterion, optimiser, epoch):
    # switch to train mode
    # model.train()
    # no BNs updates
    model.dropout3.train()
    model.dropout4.train()
    model.dropout_clf.train()
    running_loss = 0.0
    for i, sample in enumerate(train_loader):
        input = sample['image'].cuda()
        target = np.array(sample['mask'][0])
        input_var = torch.autograd.Variable(input)
        # compute output
        output = model(input_var.float())
        target = cv2.resize(target, tuple(output.size()[2:][::-1]),
                            interpolation=cv2.INTER_NEAREST)
        target_var = (torch.autograd.Variable(torch.from_numpy(target[None]))
                      .long()
                      .cuda())
        output = nn.LogSoftmax()(output)
        normal = target_var.numel()
        loss = criterion(output, target_var)
        if normal > 0:
	        loss /= normal
        running_loss += loss.data[0]
        # compute gradient and do SGD step
        optimiser.zero_grad()
        loss.backward()
        optimiser.step()

        if i % args.print_every == (args.print_every - 1):
            logger.info('Epoch: [{0}][{1}/{2}]\t'
                  'Loss {3:.4f}'.format(
                   epoch, i, len(train_loader), running_loss / args.print_every))
            running_loss = 0.0


def validate(val_loader, model, epoch, n_classes,
             label_names):

    # switch to evaluate mode
    model.eval()
    #out_dir = '{}/ep{}/'.format(args.snapshot_dir, epoch)
    #if not os.path.exists(out_dir):
    #    os.makedirs(out_dir)
    cm = np.zeros((n_classes, n_classes))
    for i, sample in enumerate(val_loader):
        input = sample['image'].cuda()
        target = sample['mask'].cuda().long()
        input_var = torch.autograd.Variable(input, volatile=True)
        target = torch.autograd.Variable(target, volatile=True)
        # compute output
        output = model(input_var.float())
        preds = cv2.resize(output[0].cpu().data.numpy().transpose((1, 2, 0)),
                            input_var.size()[2:][::-1],
                            interpolation=cv2.INTER_CUBIC)
        preds = preds[:, :, :n_classes].argmax(axis=2).reshape(-1)
        gt = target.view(-1).data.cpu().numpy()
        gt_indices = gt < n_classes
        cm += fast_cm(preds[gt_indices], gt[gt_indices], n_classes)
        if i % args.print_every == 0:
            iu = compute_iu(cm)
            miu = iu.mean()
            ius = '; '.join(map(lambda x: '{}:{:.4f}'.format(x[0], x[1]),
                                zip(label_names, iu)))
            logger.info('Test: [{0}/{1}]\t'
                        'IUs: [{2}]\t'
                        'Mean IoU: {3:.4f}'
                        .format(i,
                                len(val_loader),
                                ius,
                                miu))
    iu = compute_iu(cm)
    miu = iu.mean()
    ius = '; '.join(map(lambda x: '{}:{:.4f}'.format(x[0], x[1]),
                        zip(label_names, iu)))
    logger.info('Final IUs: [{0}]\t'
                'Final Mean IoU: {1:.4f}'
                .format(ius, miu))


def main():
    global args
    global logger
    logger = logging.getLogger(__name__)
    args = get_arguments()
	# SEEDING FOR REPRODUCIBILITY
    if torch.cuda.is_available():
        torch.cuda.manual_seed(args.random_seed)
    torch.manual_seed(args.random_seed)
    np.random.seed(args.random_seed)

    if not os.path.exists(args.snapshot_dir):
        os.makedirs(args.snapshot_dir)

    model = resnet101(pretrained=args.not_restore_new,
                      num_classes=args.num_classes)
    model.cuda()


    ## FILTER PARAMETERS ##
    base_params = []
    ref_params = []
    for p in model.named_parameters():
        if ('conv1' in p[0]) or ('layer' in p[0]) or ('bn1' in p[0]):
            base_params.append(p[1])
        else:
            ref_params.append(p[1])
    ## LOSS FUNCTION AND OPTIMISER ##
    criterion = nn.NLLLoss2d(size_average=False, ignore_index=255).cuda()
    optimiser = torch.optim.SGD([{'params' : base_params},
                                 {'params' : ref_params, 'lr' : args.learning_rate * 10}],
                                 lr=args.learning_rate, momentum=args.momentum,
                                 weight_decay=args.weight_decay)

    ## RESTORE PARAMS ##
    start_epoch = 0
    if args.restore_from is not None:
        if os.path.isfile(args.restore_from):
            checkpoint = torch.load(args.restore_from)
            pretrained_dict = checkpoint['state_dict']
            #state_dict = {k:v for k,v in checkpoint['state_dict'].iteritems() if k in model.state_dict()}
            model_dict = model.state_dict()
            # 1. filter out unnecessary keys
            pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
            # 2. overwrite entries in the existing state dict
            model_dict.update(pretrained_dict)
            pretrained_dict.update(model_dict)
            # 3. load the new state dict
            model.load_state_dict(pretrained_dict)

            #model.load_state_dict(state_dict)
            if 'epoch' in checkpoint:
                start_epoch = checkpoint['epoch']
            if 'optimiser' in checkpoint:
                optimiser.load_state_dict(checkpoint['optimiser'])
            logger.info("=> loaded checkpoint '{}' (epoch {})"
                        .format(args.restore_from, start_epoch))
        else:
            logger.info("=> no checkpoint found at '{}'".format(args.restore_from))

    ## DATA PREPARATION ##
    composed = transforms.Compose([RandomScale(SCALES, args.shorter_side),
                                   RandomMirror(),
				                   RandomCrop(args.crop_size, FINAL_SCALE),
                                   Normalize(IMG_SCALE, IMG_MEAN, IMG_STD),
                                   ToTensor(),
                                 ])
    composed_val = transforms.Compose([
                                       Normalize(IMG_SCALE, IMG_MEAN, IMG_STD),
                                       ToTensor(),
                                      ])
    trainset = PascalVOCDataset(data_file=args.data_list,
                                root_dir=args.data_dir,
                                transform=composed)
    train_loader = DataLoader(trainset, batch_size=1,
                             shuffle=True, num_workers=0, pin_memory=False)
    valset = PascalVOCDataset(data_file=args.data_val_list,
                                root_dir=args.data_dir,
                                transform=composed_val)
    val_loader = DataLoader(valset, batch_size=1, shuffle=False, num_workers=0, pin_memory=False)

    if args.evaluate:
        validate(val_loader, model, start_epoch)
        return
    model.eval() ## no updates for batch norm statistics
    for epoch in range(start_epoch, args.num_steps):
        # train for one epoch
        train(train_loader, model, criterion, optimiser, epoch)
        if (epoch + 1) % args.val_every == 0:
        # evaluate on validation set
            validate(val_loader, model, epoch + 1,args.num_classes,parameters.item_names)
        # remember best prec@1 and save checkpoint
            save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'optimiser' : optimiser.state_dict(),
	        }, epoch + 1)

if __name__ == '__main__':
    main()
