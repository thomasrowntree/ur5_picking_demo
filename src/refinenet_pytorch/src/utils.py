"""Helper functions."""

from itertools import izip

import numpy as np
import torch

def fast_cm(preds, gt, n_classes):
    """Computing confusion matrix faster.
    
    Args:
      preds (Tensor) : predictions (either flatten or of size (len(gt), top-N)).
      gt (Tensor) : flatten gt.
      n_classes (int) : number of classes.
    
    Returns:

      Confusion matrix 
      (Tensor of size (n_classes, n_classes)).
    
    """
    cm = np.zeros((n_classes, n_classes))
    for a, p in izip(gt, preds):
        cm[a][p] += 1
    return cm

def compute_iu(cm):
    """Compute IU from confusion matrix.
    
    Args:
      cm (Tensor) : square confusion matrix.
      
    Returns:
      IU vector (Tensor).
    
    """
    n_classes = cm.shape[0]
    IU = np.zeros(n_classes)
    for i in xrange(n_classes):
        pi = np.sum(cm[:, i])#.sum()
        gi = np.sum(cm[i, :])#.sum()
        ii = cm[i, i]
        denom = pi + gi - ii
        if denom > 0:
            IU[i] = ii / denom
    return IU