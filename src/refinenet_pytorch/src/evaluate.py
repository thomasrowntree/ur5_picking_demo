"""Evaluation script to compute mIoU using TF."""

from PIL import Image

import sys
import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
import argparse
from tqdm import tqdm

def get_arguments():
    """Parse all the arguments provided from the CLI.

    Returns:
      A list of parsed arguments.
    """
    parser = argparse.ArgumentParser(description="RefineNet Evaluation")
    parser.add_argument("--data-list", type=str, required=True,
                        help="Path to file list.")
    parser.add_argument("--gt-dir", type=str, required=True,
                        help="Path to gt masks")
    parser.add_argument("--masks-dir", type=str, required=True,
                        help="Path to predictions.")
    parser.add_argument("--num-classes", type=int, default=21,
                        help="Path to predictions.")
    return parser.parse_args()

def main():
    args = get_arguments()
    gt_dir = args.gt_dir
    masks_dir = args.masks_dir
    graph = tf.Graph()
    num_classes = args.num_classes
    with graph.as_default():
        gt_ph = tf.placeholder(dtype=tf.uint8, shape=[None, None])
        pred_ph = tf.placeholder(dtype=tf.uint8, shape=[None, None])
        pred = tf.reshape(pred_ph, [-1,])
        gt = tf.reshape(gt_ph, [-1,])
        weights = tf.cast(tf.less_equal(gt, num_classes - 1), tf.int32) # Ignoring all labels greater than or equal to n_classes.
        mIoU, update_op = tf.contrib.metrics.streaming_mean_iou(pred, gt, num_classes=num_classes, weights=weights)

    with open(args.data_list, 'rb') as f:
        val_files = f.readlines()

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    with tf.Session(graph=graph, config=config) as sess:
        init = tf.global_variables_initializer()    
        sess.run(init)
        sess.run(tf.local_variables_initializer())
        
        # Iterate over training steps.
        for idx, l in enumerate(val_files):
	    if '\t' in l:
		img_name = l.strip('\n').split('\t')[0][-15:-3]
	    else:
                img_name = l.strip('\n')[-15:-3]
            preds = np.load('{}/{}.npy'.format(masks_dir, idx))#img_name))
            feed_dict = { gt_ph : np.array(Image.open(os.path.join(gt_dir, '{}png'.format(img_name)))),
                        pred_ph : preds}
            _ = sess.run(update_op, feed_dict=feed_dict)
            if (idx + 1) % 300 == 0:
                print('step {:d}, mIoU: {:.3f}'.format(idx, mIoU.eval(session=sess)))
        print('Final Mean IoU: {:.5f}'.format(mIoU.eval(session=sess)))

if __name__ == '__main__':
    main()
