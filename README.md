# README #

ctrl+alt+t > alt+l > Robot (opens termininator windows with paths set for picking demo)
roslaunch ur5_picking_demo ur5_picking_demo.launch
~/catkin_ws/src/ur5_picking_demo/src/ate_machine0$ python state_machine0.py


This project allows a robot arm to identify the objects in front of it and then pick then determine the best way to pick them up and move them to their designated location.



This code is designed for a ur5 robot (Universal Robots) with a realsense SR300 camera attatched.

using refinenet pytorch a neural network has been trained from the annotated images in the capturing folder

The ROS node in the inference folder uses this network to convert images passed to it into a segmentation identifying the objects

The state machine uses this Ros node to segment images from the camera, and then picks up the objects once it has identified which object they are

The objects are then all put into their designated place (a hardcoded position)

In the depth branch of the code, there is another network trained based off depth images instead of rgb ones. Using both networks it
determines whether an object is from the dataset both networks were trained on, or if it is an unknown object.

The depth branch does this by simply treating any object both networks disagree on the identity of as unknown.

Parameters.py has variables which are called from all over the project, so that they only need to be changed in that one file.





The launch file will runa ll of these commands in one terminal, however if you need them individually for debugging they are:
roscore
rviz
roslaunch realsense_camera sr300_nodelet_rgbd.launch
/home/ur/catkin_ws/src/ur5_picking_demo/src/inference/inference_server.py
/home/ur/catkin_ws/src/ur5_picking_demo/src/ur_driver/ur_driver_lite.py

and if running on depth branch then also need to run
/home/ur/catkin_ws/src/ur5_picking_demo/src/inference/depth_inference_server.py
